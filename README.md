pakparse
======

PAKPARSE IS NOT CONCURRENCY-SAFE

[![GoDoc](https://godoc.org/gitlab.com/rigel314/pakparse?status.svg)](https://godoc.org/gitlab.com/rigel314/pakparse)

# About
pakparse is a go package that parses packets out of byte streams.  It supports
fixed data patterns and variable length fields.

# How it works
pakparse uses reflection to look at a given struct's fields and tags.  From
this, it builds a state machine for parsing these structs from a byte stream.

# To Do

  * Support passing through discarded bytes
  * Document that `pakparse:"-"` will omit a field
  * Use `count` and `len` tags for slice fields
    * `count` indicates the expected number of values
    * `len` indicates the length of each value – also valid for arrays
    * Special case for `[]byte` - use `len` as `count`
  * Document fast path cases
    * Fields that are arrays or slices of `byte` or `uint8` use a fast path for
      setting. Fields of `type X []byte` can use this fast path. Fields of type
      `[]MyInt` where `type MyInt byte` ***cannot*** use this fast path.