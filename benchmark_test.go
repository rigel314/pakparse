/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// benchmark_test.go: Benchmarks

package pakparse_test

import (
	"fmt"
	"reflect"
	"strings"
	"testing"

	"gitlab.com/rigel314/pakparse"
)

func BenchmarkVariableEncode(b *testing.B) {
	p := mustCreateParser(b, VariablePacket{}, nil)
	s := new(pakparse.State)

	onesPl := make([]byte, 257)
	v := reflect.ValueOf(&VariablePacket{
		Len:     uint16(len(onesPl)),
		Payload: onesPl,
		Crc:     0x1234,
	})

	for i := range onesPl {
		onesPl[i] = 1
	}

	for n := 0; n < b.N; n++ {
		s.Produce(p, v)
	}
}

func BenchmarkVariableDecode(b *testing.B) {
	p := mustCreateParser(b, VariablePacket{}, nil)
	s := new(pakparse.State)
	l := new(strings.Builder)

	onesPl := [257]byte{}
	for i := 0; i < 257; i++ {
		onesPl[i] = 1
	}

	for n := 0; n < b.N; n++ {
		l.Write([]byte{0x13, 0x37, 0x01, 0x01})
		l.Write(onesPl[:])
		l.Write(encode{Big}.b2(0x1234))
	}
	data := []byte(l.String())

	b.ResetTimer()
	mustConsume(b, p, s, data)
	for n := 0; n < b.N; n++ {
		if _, ok := s.Next(); !ok {
			b.Fatal("expected valid packet")
		}
	}

	// BenchmarkVariable-8         	  200000	      8121 ns/op	    2056 B/op	      21 allocs/op --- one byte at a time
	// BenchmarkVariableDecode-8   	   30000	     39353 ns/op	   10336 B/op	     802 allocs/op --- ethan's "improvements"
	// BenchmarkVariableDecode-8   	  100000	     17968 ns/op	    1072 B/op	     274 allocs/op --- many bytes at a time
	// BenchmarkVariableDecode-8   	  200000	      9904 ns/op	     816 B/op	      14 allocs/op --- generate value setter ahead of time
	// BenchmarkVariableDecode-8   	  300000	      4867 ns/op	     800 B/op	      11 allocs/op --- use v.SetBool/etc instead of v.Set
	// BenchmarkVariableDecode-8   	 1000000	      1099 ns/op	     832 B/op	      12 allocs/op --- use reflect.Copy for byte arrays
	// BenchmarkVariableDecode-8     2000000          1052 ns/op         504 B/op          9 allocs/op --- optimizing field.Consume
	// BenchmarkVariableDecode-8     2000000           776 ns/op         184 B/op          7 allocs/op --- avoid MakeSlice in unmarshaller
	// BenchmarkVariableDecode-8     3000000           585 ns/op         120 B/op          4 allocs/op --- replace atomic.Queue with simple singly-linked list
}

func BenchmarkVariableDecodeWorst(b *testing.B) {
	p := mustCreateParser(b, VariablePacket{}, nil)
	s := new(pakparse.State)

	for n := 0; n < b.N; n++ {
		s.Consume(p, []byte{0x13})
		s.Consume(p, []byte{0x37})
		s.Consume(p, []byte{0x01})
		s.Consume(p, []byte{0x01})
		for i := 0; i < 257; i++ {
			s.Consume(p, []byte{1})
		}
		s.Consume(p, []byte{0x12})
		s.Consume(p, []byte{0x34})

		if _, ok := s.Next(); !ok {
			b.Fatal("expected valid packet")
		}
	}
}

func BenchmarkVariableDecodeNoAlloc(b *testing.B) {
	p := mustCreateParser(b, VariablePacket{}, nil)
	s := new(pakparse.State)
	l := new(strings.Builder)

	onesPl := [257]byte{}
	for i := 0; i < 257; i++ {
		onesPl[i] = 1
	}

	for n := 0; n < b.N; n++ {
		l.Write([]byte{0x13, 0x37, 0x01, 0x01})
		l.Write(onesPl[:])
		l.Write(encode{Big}.b2(0x1234))
	}
	data := []byte(l.String())

	var count int
	s.Receive = func(reflect.Value) { count++ }

	b.ResetTimer()
	mustConsume(b, p, s, data)
	if count != b.N {
		b.Fatalf("expected %d valid packets, got %d", b.N, count)
	}
}

func BenchmarkFieldsDecode(b *testing.B) {
	var tests = map[int]interface{}{
		1: struct {
			A complex128
		}{},
		5: struct {
			A, B, C, D, E complex128
		}{},
		10: struct {
			A, B, C, D, E, F, G, H, I, K complex128
		}{},
		20: struct {
			A, B, C, D, E, F, G, H, I, K complex128
			L, M, N, O, P, Q, R, S, T, U complex128
		}{},
	}

	for count, typ := range tests {
		b.Run(fmt.Sprintf("%dx", count), func(b *testing.B) {
			p := mustCreateParser(b, typ, nil)
			s := new(pakparse.State)
			l := new(strings.Builder)

			for n := 0; n < b.N; n++ {
				for i := 0; i < count; i++ {
					l.Write(encode{Big}.c128(complex(float64(i), float64(i*2+1))))
				}
			}
			data := []byte(l.String())

			b.ResetTimer()
			mustConsume(b, p, s, data)
			for n := 0; n < b.N; n++ {
				if _, ok := s.Next(); !ok {
					b.Fatal("expected valid packet")
				}
			}
		})
	}

	// BenchmarkFieldsDecode/1x     10000000           240 ns/op          48 B/op          2 allocs/op
	// BenchmarkFieldsDecode/5x      3000000           489 ns/op         112 B/op          2 allocs/op
	// BenchmarkFieldsDecode/10x     2000000           787 ns/op         192 B/op          2 allocs/op
	// BenchmarkFieldsDecode/20x     1000000          1492 ns/op         352 B/op          2 allocs/op
}
