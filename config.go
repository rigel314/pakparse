/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// config.go: General configuration

package pakparse

import (
	"reflect"
)

// A CheckFunction for a packet type checks whether a packet is valid
type CheckFunction = func(packet reflect.Value, raw []byte) bool

// A LengthFunction for a field calculates the length of a field from a partial packet
type LengthFunction = func(packet reflect.Value) int

// A ParserFunction implements custom parsing for a field of a packet
type ParserFunction = func(reset bool, data []byte, state reflect.Value, packet reflect.Value) (int, reflect.Value, Status)

// VerifyCheckFunction verifies that FN is a valid packet check function for
// TYP and returns a reflection-based wrapper. See New.
//
// A valid check function is one of:
//   - func(*TYP) bool
//   - func([]byte) bool
//   - func(*TYP, []byte) bool
//   - func([]byte, *TYP) bool
func VerifyCheckFunction(typ reflect.Type, fn interface{}) (CheckFunction, error) {
	if typ.Kind() != reflect.Struct {
		return nil, ErrExpectedStructType
	}

	if x, ok := fn.(CheckFunction); ok {
		return x, nil
	}

	ptr := reflect.PtrTo(typ)
	check := reflect.ValueOf(fn)

	switch check.Type() {
	case reflect.FuncOf(types{ptr}, types{typeBool}, false):
		return func(v reflect.Value, d []byte) bool { return check.Call(values{v})[0].Bool() }, nil
	case reflect.FuncOf(types{typeBytes}, types{typeBool}, false):
		return func(v reflect.Value, d []byte) bool { return check.Call(values{reflect.ValueOf(d)})[0].Bool() }, nil
	case reflect.FuncOf(types{ptr, typeBytes}, types{typeBool}, false):
		return func(v reflect.Value, d []byte) bool { return check.Call(values{v, reflect.ValueOf(d)})[0].Bool() }, nil
	case reflect.FuncOf(types{typeBytes, ptr}, types{typeBool}, false):
		return func(v reflect.Value, d []byte) bool { return check.Call(values{reflect.ValueOf(d), v})[0].Bool() }, nil
	default:
		return nil, fmtErrorf("check function must be a function accepting %v and/or []byte and returning bool", ptr)
	}
}

// VerifyLengthFunction verifies that FN is a valid field length function for
// TYP and returns a reflection-based wrapper. See New.
//
// A valid length function is:
//   - func(*TYP) INT_TYPE
//
// INT_TYPE can be any built-in integer type.
func VerifyLengthFunction(typ reflect.Type, fn interface{}) (LengthFunction, error) {
	if typ.Kind() != reflect.Struct {
		return nil, ErrExpectedStructType
	}

	if x, ok := fn.(LengthFunction); ok {
		return x, nil
	}

	ptr := reflect.PtrTo(typ)
	err := fmtErrorf("length function must be a function accepting %v and returning an int type", ptr)

	lenfun := reflect.ValueOf(fn)
	if lenfun.Kind() != reflect.Func {
		return nil, err
	}

	ftyp := lenfun.Type()
	if ftyp.IsVariadic() || ftyp.NumIn() != 1 || ftyp.In(0) != ptr || ftyp.NumOut() != 1 {
		return nil, err
	}

	signed, unsigned := isIntType(ftyp.Out(0))
	if signed {
		return func(v reflect.Value) int { return int(lenfun.Call(values{v})[0].Int()) }, nil
	} else if unsigned {
		return func(v reflect.Value) int { return int(lenfun.Call(values{v})[0].Uint()) }, nil
	} else {
		return nil, err
	}
}

// VerifyParserFunction verifies that FN is a valid field parser function for
// TYP and returns a reflection-based wrapper. See New.
//
// A valid parser function is:
//   - func(bool, []byte, STATE, TYP) (int, STATE, Status)
//
// The first and second parameters, RESET and DATA, are mutually exclusive, when
// used by the parser. If RESET is true, DATA is nil.
//
// The types of the third parameter and second return value, STATE_IN and
// STATE_OUT, are not required to be identical, so long as STATE_OUT can be
// passed as STATE_IN in a subsequent call.
//
// The first return value, N, is the number of bytes consumed. This can be
// positive, negative, or zero. If it is negative, this indicates that values
// should be pulled off the internal buffer of processed data and reprocessed.
//
// The last return value, STATUS, is used to indicate whether field parsing is
// complete, incomplete, or invalid.
//
// Repeatedly returning N == 0 and STATUS == incomplete will stall the parser.
// The parser has checks that will cause a panic if this happens many times.
func VerifyParserFunction(typ reflect.Type, fn interface{}) (ParserFunction, error) {
	if typ.Kind() != reflect.Struct {
		return nil, ErrExpectedStructType
	}

	if x, ok := fn.(ParserFunction); ok {
		return x, nil
	}

	ptr := reflect.PtrTo(typ)
	err := fmtErrorf("parser function must be a function with the signature func(bool, []byte, STATE, %v) (int, STATE, Status)", ptr)

	parser := reflect.ValueOf(fn)
	if parser.Kind() != reflect.Func {
		return nil, err
	}

	ftyp := parser.Type()
	if ftyp.IsVariadic() || ftyp.NumIn() != 4 || ftyp.NumOut() != 3 {
		return nil, err
	}

	i0 := typeBool.AssignableTo(ftyp.In(0))
	i1 := typeBytes.AssignableTo(ftyp.In(1))
	i2 := ftyp.Out(1).AssignableTo(ftyp.In(2))
	i3 := ptr.AssignableTo(ftyp.In(3))
	o0s, o0u := isIntType(ftyp.Out(0))
	o2 := typeStatus.AssignableTo(ftyp.Out(2))
	if !i0 || !i1 || !i2 || !i3 || !(o0s || o0u) || !o2 {
		return nil, err
	}

	var conv func(reflect.Value) int
	if o0s {
		conv = func(v reflect.Value) int { return int(v.Int()) }
	} else {
		conv = func(v reflect.Value) int { return int(v.Uint()) }
	}

	return func(reset bool, data []byte, state reflect.Value, packet reflect.Value) (int, reflect.Value, Status) {
		if !state.IsValid() {
			state = reflect.Zero(ftyp.Out(1))
		}
		out := parser.Call(values{reflect.ValueOf(reset), reflect.ValueOf(data), state, packet})
		return conv(out[0]), out[1], Status(out[2].Int())
	}, nil
}
