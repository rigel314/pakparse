/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// decode_test.go: Tests packet parsing

package pakparse_test

import (
	"math"
	"strings"
	"testing"
	"time"

	assert_ "github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/rigel314/pakparse"
)

// TODO: test maxlen on non 8-bit slice type

func TestPointerFieldDecode(t *testing.T) {
	type Inner struct{ V int16 }
	type Packet struct{ U *Inner }

	p := mustCreateParser(t, Packet{}, nil)

	s := new(pakparse.State)
	mustConsume(t, p, s, encode{Big}.b2(0x1337))

	requirePacket(t, s, Packet{&Inner{0x1337}})
}

func TestSimpleIdealDecode(t *testing.T) {
	p := mustCreateParser(t, SimplePacket{}, nil)

	s := new(pakparse.State)
	mustConsume(t, p, s, encode{Big}.b2(0x1337))
	mustConsume(t, p, s, encode{Big}.f64(math.Pi))
	mustConsume(t, p, s, encode{Big}.f64(math.Phi))
	mustConsume(t, p, s, encode{Big}.f64(math.E))
	mustConsume(t, p, s, encode{Big}.f64(math.Sqrt2))
	mustConsume(t, p, s, encode{Big}.b2(0xDEAD))

	requirePacket(t, s, SimplePacket{
		Magic: 0x1337,
		A:     math.Pi,
		B:     math.Phi,
		C:     math.E,
		D:     math.Sqrt2,
		Crc:   0xDEAD,
	})
}

func TestEmbeddedField(t *testing.T) {
	p := mustCreateParser(t, EmbeddedFieldPacket{}, nil)

	s := new(pakparse.State)
	mustConsume(t, p, s, encode{Big}.b2(0x1337))
	mustConsume(t, p, s, encode{Big}.f64(math.Pi))
	mustConsume(t, p, s, encode{Big}.f64(math.Phi))
	mustConsume(t, p, s, encode{Big}.f64(math.E))
	mustConsume(t, p, s, encode{Big}.f64(math.Sqrt2))
	mustConsume(t, p, s, encode{Big}.b2(0xDEAD))

	requirePacket(t, s, EmbeddedFieldPacket{SimplePacket{
		Magic: 0x1337,
		A:     math.Pi,
		B:     math.Phi,
		C:     math.E,
		D:     math.Sqrt2,
		Crc:   0xDEAD,
	}})
}

func TestSimpleIdealUnmarshal(t *testing.T) {
	b := new(strings.Builder)
	b.Write(encode{Big}.b2(0x1337))
	b.Write(encode{Big}.f64(math.Pi))
	b.Write(encode{Big}.f64(math.Phi))
	b.Write(encode{Big}.f64(math.E))
	b.Write(encode{Big}.f64(math.Sqrt2))
	b.Write(encode{Big}.b2(0xDEAD))

	p := mustCreateParser(t, SimplePacket{}, nil)

	var v SimplePacket
	mustUnmarshal(t, p, []byte(b.String()), &v)
	require.Equal(t, SimplePacket{
		Magic: 0x1337,
		A:     math.Pi,
		B:     math.Phi,
		C:     math.E,
		D:     math.Sqrt2,
		Crc:   0xDEAD,
	}, v)
}

func TestSimpleIdealManyDecode(t *testing.T) {
	const N = 10

	p := mustCreateParser(t, SimplePacket{}, nil)

	s := new(pakparse.State)
	b := &strings.Builder{}

	// write N packets to the buffer
	for i := 0; i < N; i++ {
		b.Write(encode{Big}.b2(0x1337))
		b.Write(encode{Big}.f64(float64(i)))
		b.Write(encode{Big}.f64(math.Phi))
		b.Write(encode{Big}.f64(math.E))
		b.Write(encode{Big}.f64(math.Sqrt2 * float64(i+1)))
		b.Write(encode{Big}.b2(0xDEAD))
	}

	// process all data at once
	mustConsume(t, p, s, []byte(b.String()))

	for i := 0; i < N; i++ {
		t.Log("packet", i)
		requirePacket(t, s, SimplePacket{
			Magic: 0x1337,
			A:     float64(i),
			B:     math.Phi,
			C:     math.E,
			D:     math.Sqrt2 * float64(i+1),
			Crc:   0xDEAD,
		})
	}
}

func TestSimpleBeginningGarbageDecode(t *testing.T) {
	p := mustCreateParser(t, SimplePacket{}, nil)

	s := new(pakparse.State)
	mustConsume(t, p, s, []byte{0x00, 0x13, 0x00, 0x13, 0x37})
	mustConsume(t, p, s, encode{Big}.f64(math.Pi))
	mustConsume(t, p, s, encode{Big}.f64(math.Phi))
	mustConsume(t, p, s, encode{Big}.f64(math.E))
	mustConsume(t, p, s, encode{Big}.f64(math.Sqrt2))
	mustConsume(t, p, s, encode{Big}.b2(0xDEAD))

	requirePacket(t, s, SimplePacket{
		Magic: 0x1337,
		A:     math.Pi,
		B:     math.Phi,
		C:     math.E,
		D:     math.Sqrt2,
		Crc:   0xDEAD,
	})
}

func TestSimpleBeginningDoubleSyncDecode(t *testing.T) {
	p := mustCreateParser(t, SimplePacket{}, nil)

	s := new(pakparse.State)
	mustConsume(t, p, s, []byte{0x13, 0x13, 0x37})
	mustConsume(t, p, s, encode{Big}.f64(math.Pi))
	mustConsume(t, p, s, encode{Big}.f64(math.Phi))
	mustConsume(t, p, s, encode{Big}.f64(math.E))
	mustConsume(t, p, s, encode{Big}.f64(math.Sqrt2))
	mustConsume(t, p, s, encode{Big}.b2(0xDEAD))

	requirePacket(t, s, SimplePacket{
		Magic: 0x1337,
		A:     math.Pi,
		B:     math.Phi,
		C:     math.E,
		D:     math.Sqrt2,
		Crc:   0xDEAD,
	})
}

func TestSimpleCheckFunctionDecode(t *testing.T) {
	p := mustCreateParser(t, SimplePacket{}, &pakparse.Config{
		Check: func(s *SimplePacket) bool { return s.Crc == 0xDEAD },
	})

	s := new(pakparse.State)
	mustConsume(t, p, s, encode{Big}.b2(0x1337))
	mustConsume(t, p, s, encode{Big}.f64(math.Pi))
	mustConsume(t, p, s, encode{Big}.f64(math.Phi))
	mustConsume(t, p, s, encode{Big}.f64(math.E))
	mustConsume(t, p, s, encode{Big}.f64(math.Sqrt2))
	mustConsume(t, p, s, encode{Big}.b2(0xDEAD))
	mustConsume(t, p, s, encode{Big}.b2(0x1337))
	mustConsume(t, p, s, encode{Big}.f64(math.Phi))
	mustConsume(t, p, s, encode{Big}.f64(math.Pi))
	mustConsume(t, p, s, encode{Big}.f64(math.E))
	mustConsume(t, p, s, encode{Big}.f64(math.Sqrt2))
	mustConsume(t, p, s, encode{Little}.b2(0xDEAD))

	requirePacket(t, s, SimplePacket{
		Magic: 0x1337,
		A:     math.Pi,
		B:     math.Phi,
		C:     math.E,
		D:     math.Sqrt2,
		Crc:   0xDEAD,
	})

	_, ok := s.Next()
	require.False(t, ok, "got two packets")
}

func TestByteCheckFunctionDecode(t *testing.T) {
	bld := &strings.Builder{}
	bld.Write(encode{Big}.b2(0x1337))
	bld.Write(encode{Big}.f64(math.Pi))
	bld.Write(encode{Big}.f64(math.Phi))
	bld.Write(encode{Big}.f64(math.E))
	bld.Write(encode{Big}.f64(math.Sqrt2))
	bld.Write(encode{Big}.b2(0xDEAD))

	p := mustCreateParser(t, SimplePacket{}, &pakparse.Config{
		Check: func(b []byte) bool {
			return assert_.Equal(t, []byte(bld.String()), b)
		},
	})

	s := new(pakparse.State)
	mustConsume(t, p, s, []byte(bld.String()))

	requirePacket(t, s, SimplePacket{
		Magic: 0x1337,
		A:     math.Pi,
		B:     math.Phi,
		C:     math.E,
		D:     math.Sqrt2,
		Crc:   0xDEAD,
	})
}

func TestSliceIdealDecode(t *testing.T) {
	p := mustCreateParser(t, SimpleSlicePacket{}, nil)

	s := new(pakparse.State)
	mustConsume(t, p, s, encode{Big}.b2(0x1337))
	mustConsume(t, p, s, encode{Big}.f64(math.Pi))
	mustConsume(t, p, s, encode{Big}.f64(math.Phi))
	mustConsume(t, p, s, encode{Big}.f64(math.E))
	mustConsume(t, p, s, encode{Big}.f64(math.Sqrt2))
	mustConsume(t, p, s, encode{Big}.b2(0xDEAD))

	requirePacket(t, s, SimpleSlicePacket{
		Magic: 0x1337,
		Q: []float64{
			math.Pi,
			math.Phi,
			math.E,
			math.Sqrt2,
		},
		Crc: 0xDEAD,
	})
}

func TestVariableIdealDecode(t *testing.T) {
	p := mustCreateParser(t, VariablePacket{}, nil)

	s := new(pakparse.State)
	onesPl := [257]byte{}
	for i := 0; i < 257; i++ {
		onesPl[i] = 1
	}
	mustConsume(t, p, s, []byte{0x13, 0x37, 0x01, 0x01})
	mustConsume(t, p, s, onesPl[:])
	mustConsume(t, p, s, encode{Big}.b2(0x1234))

	requirePacket(t, s, VariablePacket{
		Magic:   0x1337,
		Len:     257,
		Payload: onesPl[:],
		Crc:     0x1234,
	})
}

func TestVariableZeroLenDecode(t *testing.T) {
	p := mustCreateParser(t, VariablePacket{}, nil)

	s := new(pakparse.State)
	mustConsume(t, p, s, []byte{0x13, 0x37, 0x00, 0x00})
	mustConsume(t, p, s, encode{Big}.b2(0x1234))

	requirePacket(t, s, VariablePacket{
		Magic:   0x1337,
		Crc:     0x1234,
		Payload: []byte{},
	})
}

func TestVariableLenFuncDecode(t *testing.T) {
	p := mustCreateParser(t, VariablePacket{}, &pakparse.Config{
		Count: map[string]interface{}{
			"Payload": func(v *VariablePacket) uint16 { return v.Len + 1 },
		},
	})

	onesPl := [16]byte{}
	for i := 0; i < 16; i++ {
		onesPl[i] = 1
	}

	s := new(pakparse.State)
	mustConsume(t, p, s, []byte{0x13, 0x37, 0x00, 0x0F})
	mustConsume(t, p, s, onesPl[:])
	mustConsume(t, p, s, encode{Big}.b2(0x1234))

	requirePacket(t, s, VariablePacket{
		Magic:   0x1337,
		Len:     15,
		Payload: onesPl[:],
		Crc:     0x1234,
	})
}

func TestVariableMaxlenDecode(t *testing.T) {
	p := mustCreateParser(t, VariablePacket{}, nil)

	s := new(pakparse.State)
	onesPl := [260]byte{}
	for i := 0; i < 260; i++ {
		onesPl[i] = 1
	}
	mustConsume(t, p, s, []byte{0x13, 0x37, 0x01, 0x04})
	mustConsume(t, p, s, onesPl[:])
	mustConsume(t, p, s, encode{Big}.b2(0x1234))

	requirePacket(t, s, VariablePacket{
		Magic:   0x1337,
		Len:     260,
		Payload: onesPl[:],
		Crc:     0x1234,
	})
}

func TestVariableMaxlenOverDecode(t *testing.T) {
	p := mustCreateParser(t, VariablePacket{}, nil)

	onesPl := [261]byte{}
	for i := 0; i < 261; i++ {
		onesPl[i] = 1
	}

	s := new(pakparse.State)
	mustConsume(t, p, s, []byte{0x13, 0x37, 0x01, 0x05})
	mustConsume(t, p, s, onesPl[:])
	mustConsume(t, p, s, encode{Big}.b2(0x1234))

	_, ok := s.Next()
	require.False(t, ok, "received packet when shouldn't have")
}

func TestVariableDelimiterIdealDecode(t *testing.T) {
	p := mustCreateParser(t, DelimiterPacket{}, nil)

	onesPl := [16]byte{}
	for i := 0; i < 16; i++ {
		onesPl[i] = 1
	}

	s := new(pakparse.State)
	mustConsume(t, p, s, []byte{0x7E, 0x02})
	mustConsume(t, p, s, onesPl[:])
	mustConsume(t, p, s, []byte{0x12, 0x34, 0x7E})

	requirePacket(t, s, DelimiterPacket{
		Start:    0x7E,
		PacketID: 2,
		Payload:  onesPl[:],
		Checksum: 0x1234,
		End:      0x7E,
	})
}

func TestVariableLongDelimiterDecode(t *testing.T) {
	type Packet struct {
		Start   uint8 `pakparse:"value=0x7E"`
		Payload []byte
		End     uint64 `pakparse:"value=0x7A7B7C7D7E"`
	}

	p := mustCreateParser(t, Packet{}, nil)

	onesPl := [16]byte{}
	for i := 0; i < 16; i++ {
		onesPl[i] = 1
	}

	s := new(pakparse.State)
	mustConsume(t, p, s, []byte{0x7E})
	mustConsume(t, p, s, onesPl[:])
	mustConsume(t, p, s, []byte{0x7A})
	mustConsume(t, p, s, []byte{0x7B})
	mustConsume(t, p, s, []byte{0x7C})
	mustConsume(t, p, s, []byte{0x7D})
	mustConsume(t, p, s, []byte{0x7E})

	requirePacket(t, s, Packet{
		Start:   0x7E,
		Payload: onesPl[:],
		End:     0x7A7B7C7D7E,
	})
}

func TestVariableDelimiterDoublingDecode(t *testing.T) {
	t.Skip("handling delimiter doubling is not implemented")

	p := mustCreateParser(t, DelimiterPacket{}, nil)

	onesPl := [16]byte{}
	for i := 0; i < 16; i++ {
		onesPl[i] = 1
	}

	s := new(pakparse.State)
	mustConsume(t, p, s, []byte{0x7E, 0x02})
	mustConsume(t, p, s, onesPl[:])
	mustConsume(t, p, s, []byte{0x7E, 0x7E, 0x12, 0x34, 0x7E})

	requirePacket(t, s, DelimiterPacket{
		Start:    0x7E,
		PacketID: 2,
		Payload:  append(onesPl[:], 0x7E),
		Checksum: 0x1234,
		End:      0x7E,
	})
}

func TestVariableDelimiterMaxlenDecode(t *testing.T) {
	p := mustCreateParser(t, DelimiterPacket{}, nil)

	onesPl := [20]byte{}
	for i := 0; i < 20; i++ {
		onesPl[i] = 1
	}

	s := new(pakparse.State)
	mustConsume(t, p, s, []byte{0x7E, 0x37})
	mustConsume(t, p, s, onesPl[:])
	mustConsume(t, p, s, []byte{0x12, 0x34, 0x7E})

	requirePacket(t, s, DelimiterPacket{
		Start:    0x7E,
		PacketID: 0x37,
		Payload:  onesPl[:],
		Checksum: 0x1234,
		End:      0x7E,
	})
}

func TestVariableDelimiterMaxlenOverDecode(t *testing.T) {
	p := mustCreateParser(t, DelimiterPacket{}, nil)

	onesPl := [21]byte{}
	for i := 0; i < 21; i++ {
		onesPl[i] = 1
	}

	s := new(pakparse.State)
	mustConsume(t, p, s, []byte{0x7E, 0x37})
	mustConsume(t, p, s, onesPl[:])
	mustConsume(t, p, s, []byte{0x12, 0x34, 0x7E})

	_, ok := s.Next()
	require.False(t, ok, "received packet when shouldn't have")
}

func TestEndianDecode(t *testing.T) {
	p := mustCreateParser(t, EndianTestPacket{}, nil)

	s := new(pakparse.State)
	mustConsume(t, p, s, encode{Big}.b2(0x1234))
	mustConsume(t, p, s, []byte{0x34, 0x12})

	requirePacket(t, s, EndianTestPacket{
		D1: 0x1234,
		D2: 0x1234,
	})
}

func TestFixedEndianDecode(t *testing.T) {
	p := mustCreateParser(t, FixedEndianTestPacket{}, nil)

	s := new(pakparse.State)
	mustConsume(t, p, s, encode{Big}.b2(0x1234))
	mustConsume(t, p, s, []byte{0x34, 0x12})

	requirePacket(t, s, FixedEndianTestPacket{
		D1: 0x1234,
		D2: 0x1234,
	})
}

func TestWideIntDecode(t *testing.T) {
	p := mustCreateParser(t, WideIntPacket{}, nil)

	s := new(pakparse.State)
	mustConsume(t, p, s, []byte{0x12, 0x34, 0x56})

	requirePacket(t, s, WideIntPacket{
		D1: 0x123456,
	})
}

func TestBadSyncDecode(t *testing.T) {
	p := mustCreateParser(t, SimpleSyncPacket{}, nil)

	s := new(pakparse.State)
	mustConsume(t, p, s, []byte{0x13, 0x37, 0x37, 0xAB, 0xCD})

	_, ok := s.Next()
	require.False(t, ok, "received packet when shouldn't have")
}

func TestRecursionCheck(t *testing.T) {
	p := mustCreateParser(t, SimpleSyncPacket{}, &pakparse.Config{
		Parser: map[string]interface{}{
			"Sync": func(bool, []byte, interface{}, *SimpleSyncPacket) (int, interface{}, pakparse.Status) {
				// ensure unbounded looping
				return 0, nil, pakparse.Invalid
			},
		},
	})

	mustRunInTime(t, time.Second, func() {
		requirePanicsWithError(t, "stalled", func() {
			new(pakparse.State).Consume(p, []byte{0x00})
		})
	}, "timeout")
}

func TestBadCustomParser(t *testing.T) {
	p := mustCreateParser(t, SimpleSyncPacket{}, &pakparse.Config{
		Parser: map[string]interface{}{
			"Sync": func(bool, []byte, interface{}, *SimpleSyncPacket) (int, interface{}, pakparse.Status) {
				return -1, nil, pakparse.Invalid
			},
		},
	})

	requirePanicsWithError(t, "parser rewound more bytes than have been read", func() {
		new(pakparse.State).Consume(p, []byte{0x00})
	})
}

func TestMaxReadSanityCheck(t *testing.T) {
	p := mustCreateParser(t, SimpleSyncPacket{}, &pakparse.Config{
		Parser: map[string]interface{}{
			"Sync": func(bool, []byte, interface{}, *SimpleSyncPacket) (int, interface{}, pakparse.Status) {
				return 1000000, nil, pakparse.Invalid
			},
		},
	})

	requirePanicsWithError(t, "parser read more bytes than were provided for its consumption", func() {
		new(pakparse.State).Consume(p, []byte{0x00})
	})
}
