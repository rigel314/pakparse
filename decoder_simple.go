/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// decoder_simple.go: Basic reader-based decoder implementation

package pakparse

import "io"

type simpleDecoder struct {
	source io.Reader

	zeroCount int
	buf       [1 << 13]byte
}

func (d *simpleDecoder) read() (int, error) {
	n, err := d.source.Read(d.buf[:])
	if terr, ok := err.(interface{ Timeout() bool }); ok && terr.Timeout() {
		return 0, nil
	}
	if err != nil {
		return 0, err
	}

	if n == 0 {
		if d.zeroCount > MaxZeroByteReadCount {
			return 0, ErrReadZeroBytes
		}
		d.zeroCount++
		return 0, nil
	}

	d.zeroCount = 0
	return n, nil
}

func (d *simpleDecoder) decode(p *Parser, s *State) error {
	n, err := d.read()
	if err != nil {
		return err
	} else if n == 0 {
		return nil
	}

	// process data
	s.Consume(p, d.buf[:n])

	return nil
}
