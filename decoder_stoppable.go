/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// decoder_stoppable.go: Reader-based decoder implementation with support for timing out and stopping

package pakparse

import (
	"context"
	"io"
	"sync"
	"time"
)

type stoppableDecoder struct {
	timeoutDecoder

	context  context.Context
	cancel   context.CancelFunc
	doneSync sync.WaitGroup

	setDeadline func(t time.Time) error
	readTimeout time.Duration
}

func (d *stoppableDecoder) setReadTimeout(timeout time.Duration) {
	d.readTimeout = timeout
}

func (d *stoppableDecoder) start(r io.Reader, started func()) {
	d.doneSync.Add(1)
	d.source = make(chan []byte)
	d.simpleDecoder.source = r
	d.context, d.cancel = context.WithCancel(context.Background())

	defer d.stop()
	defer d.doneSync.Done()
	defer func() {
		close(d.source)
	}()

	started()
	for {
		select {
		case <-d.context.Done():
			d.err = ErrClosed
			return
		default:
		}

		err := d.read()
		if err != nil {
			d.err = err
			return
		}
	}
}

func (d *stoppableDecoder) stop() {
	d.cancel()
	d.doneSync.Wait()
}

func (d *stoppableDecoder) read() error {
	if d.readTimeout == 0 {
		d.readTimeout = 1 * time.Second
	}

	// set a read deadline, to allow this routine to die if closed
	err := d.setDeadline(time.Now().Add(d.readTimeout))
	if err != nil {
		return err
	}

	return d.timeoutDecoder.read()
}
