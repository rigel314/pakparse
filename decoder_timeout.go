/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// decoder_timeout.go: Reader-based decoder implementation with support for timing out

package pakparse

import (
	"io"
	"sync"
	"time"
)

type timeoutDecoder struct {
	simpleDecoder

	err error

	source     chan []byte
	sourceSync sync.WaitGroup

	timeout time.Duration
}

func (d *timeoutDecoder) start(r io.Reader, started func()) {
	d.source = make(chan []byte)
	d.simpleDecoder.source = r

	defer func() {
		close(d.source)
	}()

	started()
	for {
		err := d.read()
		if err != nil {
			d.err = err
			return
		}
	}
}

func (d *timeoutDecoder) read() error {
	n, err := d.simpleDecoder.read()
	if n == 0 || err != nil {
		return err
	}

	// send data to Decode, wait for Decode to finish
	//   synchronization or copying is necessary to avoid races
	d.sourceSync.Add(1)
	d.source <- d.buf[:n]
	d.sourceSync.Wait()
	return nil
}

func (d *timeoutDecoder) decode(p *Parser, s *State) error {
	fn := func(data []byte, ok bool) error {
		if !ok {
			return d.err
		}

		// process data
		s.Consume(p, data)

		// free the IO runloop
		d.sourceSync.Done()

		return nil
	}

	if d.timeout == 0 {
		data, ok := <-d.source
		return fn(data, ok)
	}

	select {
	case <-time.After(d.timeout):
		// reset the parser
		s.Reset(p)
		return nil

	case data, ok := <-d.source:
		return fn(data, ok)
	}
}

func (d *timeoutDecoder) setTimeout(t time.Duration) {
	d.timeout = t
}
