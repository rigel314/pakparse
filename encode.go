/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// encode.go: Packet encoding implementation

package pakparse

import (
	"bytes"
	"io"
	"reflect"
	"strings"
)

// func (s *State) SetCheckCalcFunction(f interface{}) {
// 	// TODO: panic if not a function that takes a struct of type outType and returns a struct of type outType
// 	s.checkCalcF = reflect.ValueOf(f)
// }

// Encode will serialize v to bytes then write to w as one write.  Encode will be the inverse of the parser
// used by Decode.  v must be the same type as the type the State was created with.  Any fixed data fields
// will always use the fixed data value in the struct tag, regardless of the actual field value in v.
func (s *State) Encode(p *Parser, w io.Writer, v interface{}) error {
	b, err := s.Produce(p, reflect.ValueOf(v))
	if err != nil {
		return err
	}

	// Single write to output writer
	_, err = bytes.NewBuffer(b).WriteTo(w)
	return err
}

// Produce serializes `v` to bytes using `p`.
func (s *State) Produce(p *Parser, v reflect.Value) ([]byte, error) {
	b := new(strings.Builder)

	// TODO: figure out how to callback to a user function to fill in a CRC or other check field

	for _, field := range p.fields {
		f, err := field.Produce(v)
		if err != nil {
			return nil, err
		}

		b.Write(f)
	}

	return []byte(b.String()), nil
}

// Marshal serializes `v` to bytes using `p`
func Marshal(p *Parser, v interface{}) ([]byte, error) {
	return new(State).Produce(p, reflect.ValueOf(v))
}
