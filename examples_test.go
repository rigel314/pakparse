/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// examples_test.go: Various examples of packet parsing

package pakparse_test

import (
	"fmt"
	"io"
	"log"
	"reflect"
	"time"

	"gitlab.com/go-utils/io/iochan"
	"gitlab.com/rigel314/pakparse"
)

func assert(cond bool, msg string) {
	if !cond {
		log.Fatal(msg)
	}
}

func ExampleConfig_Check() {
	c := new(pakparse.Config).Safe()

	c.Check = func(vp *ExampleVariablePacket) bool {
		fmt.Printf("checksum: 0x%04x\n", vp.Checksum)
		// A more realistic validation would be to actually calculate a checksum and compare against the received Checksum
		return vp.Checksum == 0x1234
	}

	p, err := pakparse.New(reflect.TypeOf(ExampleVariablePacket{}), c)
	if err != nil {
		log.Fatal(err)
	}

	// send some test data
	s := new(pakparse.State)
	s.Consume(p, []byte{0x13, 0x37, 0x00, 0x01})
	s.Consume(p, []byte{0x01})
	s.Consume(p, []byte{0x12, 0x34})

	// retrieve the parsed packet
	_, ok := s.Next()
	assert(ok, "Expected a valid packet")

	// Output: checksum: 0x1234
}

func ExampleConfig_Count() {
	c := new(pakparse.Config).Safe()

	c.Count["Payload"] = func(x *ExampleVariablePacket) uint16 {
		// len on the wire might be more than actual len
		fmt.Println("wire len")
		fmt.Println(x.Len)
		return x.Len - 2
	}

	p, err := pakparse.New(reflect.TypeOf(ExampleVariablePacket{}), c)
	if err != nil {
		log.Fatal(err)
	}

	// send some test data
	s := new(pakparse.State)
	s.Consume(p, []byte{0x13, 0x37, 0x00, 0x03})
	s.Consume(p, []byte{0x01})
	s.Consume(p, []byte{0x12, 0x34})

	// retrieve the parsed packet
	_, ok := s.Next()
	assert(ok, "Expected a valid packet")

	// Output:
	// wire len
	// 3
}

func ExampleDecoder_Close() {
	r, w := iochan.Pipe(0)
	defer w.Close()

	p, err := pakparse.New(reflect.TypeOf(ExampleVariablePacket{}), nil)
	if err != nil {
		log.Fatal(err)
	}

	d := pakparse.NewDecoder(p, r)
	assert(d.SetReadTimeout(1*time.Millisecond) == nil, "Did not expect an error")

	done := make(chan struct{})
	go func() {
		<-time.After(time.Millisecond)
		assert(d.Close() == nil, "Did not expect an error")
		close(done)
	}()

	var v ExampleVariablePacket
	err = d.Decode(&v)
	assert(err == io.EOF, "Expected EOF")

	<-done

	fmt.Println(err)
	// Output: EOF
}
