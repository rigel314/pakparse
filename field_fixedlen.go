/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// field_fixedlen.go: Fixed-length field parsing

package pakparse

import (
	"encoding/binary"
	"reflect"
)

type fixedLenFieldParser struct {
	structFieldParser
	length int
}

var _ FieldParser = new(fixedLenFieldParser)

func (f *fixedLenFieldParser) Size() (int, bool) { return f.length, true }

func (f *fixedLenFieldParser) Consume(data []byte, state interface{}, packet reflect.Value) (int, interface{}, Status) {
	have, need := len(data), f.length
	got, _ := state.([]byte)

	if got != nil {
		need -= len(got)

		if have < need {
			got = append(got, data[:have]...)
			return have, got, Incomplete
		}

		got = append(got, data[:need]...)
		f.unmarshalValue(got, packet)
		return need, nil, Complete
	}

	if have >= need {
		f.unmarshalValue(data[:need], packet)
		return need, nil, Complete
	}

	got = make([]byte, have, need)
	copy(got, data)
	return have, got, Incomplete
}

func (f *fixedLenFieldParser) Produce(packet reflect.Value) ([]byte, error) {
	b := f.marshalValue(packet)
	if f.length == len(b) {
		return b, nil
	}

	if f.config.Order == binary.LittleEndian {
		return b[:f.length], nil
	}

	return b[len(b)-f.length:], nil
}
