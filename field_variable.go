/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// field_variable.go: Variable-length field parsing

package pakparse

import (
	"bytes"
	"reflect"
)

type variableFieldParser struct {
	structFieldParser
}

func (v *variableFieldParser) HasMax() bool      { return v.config.MaxCount > 0 }
func (v *variableFieldParser) Max() int          { return v.config.MaxCount }
func (v *variableFieldParser) Size() (int, bool) { return -1, false }

func (v *variableFieldParser) Produce(packet reflect.Value) ([]byte, error) {
	return v.marshalValue(packet), nil
}

type variableFieldParserWithCount struct {
	variableFieldParser
	callback LengthFunction
}

func (v *variableFieldParserWithCount) Consume(data []byte, state interface{}, packet reflect.Value) (int, interface{}, Status) {
	have := len(data)
	got, _ := state.([]byte)

	if got != nil {
		need := cap(got) - len(got)

		if have < need {
			got = append(got, data[:have]...)
			return have, got, Incomplete
		}

		got = append(got, data[:need]...)
		v.unmarshalValue(got, packet)
		return need, nil, Complete
	}

	need := v.callback(packet)
	if v.HasMax() && need > v.Max() {
		// TODO: inform user?
		return 0, nil, Invalid
	}

	if have >= need {
		v.unmarshalValue(data[:need], packet)
		return need, nil, Complete
	}

	got = make([]byte, have, need)
	copy(got, data)
	return have, got, Incomplete
}

type variableFieldParserWithStop struct {
	variableFieldParser
	intermediateWidth int
	stopSequence      []byte
}

func (v *variableFieldParserWithStop) SetStop(intermediate int, sequence []byte) {
	v.intermediateWidth = intermediate
	v.stopSequence = sequence
}

func (v *variableFieldParserWithStop) Reset(interface{}, reflect.Value) interface{} {
	if v.stopSequence == nil {
		panic(fmtErrorf("missing stop sequence"))
	}
	return []byte{}
}

func (v *variableFieldParserWithStop) Consume(data []byte, state interface{}, packet reflect.Value) (int, interface{}, Status) {
	got := state.([]byte)
	tail := v.intermediateWidth + len(v.stopSequence)

	for i, b := range data {
		if v.HasMax() && len(got) > v.Max()+tail-1 {
			return i, got, Invalid
		}

		got = append(got, b)

		if bytes.HasSuffix(got, v.stopSequence) {
			if tail > len(got) {
				return i + 1, got, Invalid
			}

			v.unmarshalValue(got[:len(got)-tail], packet)
			return i + 1 - tail, got, Complete
		}
	}
	return len(data), got, Incomplete
}
