/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// fields.go: Field parser initialization

package pakparse

import (
	"encoding/binary"
	"reflect"
)

// A FieldParser parses bytes into a field
//
// Reset() and Consume() are called by State.Consume(). The parser state
// maintains a state value for each field parser. Each time field.Reset() or
// field.Consume() is called, the state value for that field is loaded, and the
// returned state value is stored.
type FieldParser interface {
	// Name returns the name of the field
	Name() string

	// Size returns the size of the field
	//
	// Size() must return (n > 0, true) for a parser representing a fixed length
	// field. Size() must return (-1, false) for a parser representing a
	// variable-length field.
	Size() (n int, fixed bool)

	// Reset resets the state
	Reset(state interface{}, packet reflect.Value) (nstate interface{})

	// Consume consumes bytes and advances the parser
	Consume(data []byte, state interface{}, packet reflect.Value) (n int, nstate interface{}, status Status)

	// Produce produces bytes from the field value
	Produce(packet reflect.Value) ([]byte, error)
}

// ParseField calls ParseFieldConfig and NewField.
func ParseField(typ reflect.Type, field reflect.StructField) (FieldParser, error) {
	config, err := ParseFieldConfig(typ, field)
	if err != nil {
		return nil, err
	}

	return NewField(typ, field, config)
}

// NewField creates a field parser.
//
// NOTE, NewField expects `config` to be generated via ParseFieldConfig. It is
// much safer to call ParseFieldConfig and modify the result than to manually
// create `config`.
//
// `config.Parser` will always override all other parsing.
func NewField(typ reflect.Type, field reflect.StructField, config *FieldConfig) (FieldParser, error) {
	if len(field.Index) != 1 {
		return nil, fmtErrorf("nested fields are not supported")
	}

	_, _, err := verifyFieldKind(field.Type)
	if err != nil {
		return nil, err
	}

	sf := &structFieldParser{
		name:   field.Name,
		idx:    field.Index[0],
		config: config,
	}

	if config.Parser != nil {
		return &funcFieldParser{*sf, config.Parser, nil}, nil
	}

	if field.Type.Kind() == reflect.Struct {
		ip, err := New(field.Type, nil)
		if err != nil {
			return nil, err
		}

		return &funcFieldParser{*sf, ip.AsFieldParser(field), ip.AsFieldMarshaller(field)}, nil
	}

	if field.Type.Kind() == reflect.Ptr && field.Type.Elem().Kind() == reflect.Struct {
		ip, err := New(field.Type.Elem(), nil)
		if err != nil {
			return nil, err
		}

		return &funcFieldParser{*sf, ip.AsFieldParser(field), ip.AsFieldMarshaller(field)}, nil
	}

	if (field.Type.Kind() == reflect.Slice || field.Type.Kind() == reflect.Array) && field.Type.Elem().Kind() == reflect.Struct {
		ip, err := New(field.Type.Elem(), nil)
		if err != nil {
			return nil, err
		}
		n := &nestedArrayParser{*sf, ip}

		if config.Count.CanCalculate() {
			return &nestedArrayParserWithCount{*n}, nil
		}

		return nil, fmtErrorf("undefined count for field %s type %v is not supported", field.Name, field.Type)
		// return &nestedArrayParserWithStop{*n, 0, nil}, nil
	}

	if err != nil {
		return nil, err
	}

	// if the user provided a data, this must be a fixedField
	if config.Value != nil {
		d := config.Value.Bytes()

		// reverse value for little endian
		if sf.Order() == binary.LittleEndian {
			for left, right := 0, len(d)-1; left < right; left, right = left+1, right-1 {
				d[left], d[right] = d[right], d[left]
			}
		}

		return &fixedValueFieldParser{*sf, d}, nil
	}

	v := &variableFieldParser{*sf}

	if !config.Width.CanCalculate() {
		if isScalarOrStringType(field.Type) {
			return &variableFieldParserWithStop{*v, 0, nil}, nil
		}

		return nil, fmtErrorf("undefined width for field %s type %v is not supported", field.Name, field.Type)
	}

	if !config.Width.IsFixed() {
		if isScalarOrStringType(field.Type) {
			return &variableFieldParserWithCount{*v, config.Width.Func}, nil
		}

		return nil, fmtErrorf("variable width for field %s type %v is not supported", field.Name, field.Type)
	}

	width := config.Width.Calculate(reflect.Value{})

	if config.Count.IsFixed() {
		count := config.Count.Calculate(reflect.Value{})
		return &fixedLenFieldParser{*sf, width * count}, nil
	}

	if isScalarOrStringType(field.Type) {
		return &fixedLenFieldParser{*sf, width}, nil
	}

	if !config.Count.CanCalculate() {
		return &variableFieldParserWithStop{*v, 0, nil}, nil
	}

	if width == 1 {
		return &variableFieldParserWithCount{*v, config.Count.Func}, nil
	}

	cfn := config.Count.Func
	fn := func(packet reflect.Value) int { return cfn(packet) * width }
	return &variableFieldParserWithCount{*v, fn}, nil
}
