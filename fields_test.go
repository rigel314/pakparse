/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// fields_test.go: Field parsing tests

package pakparse_test

import (
	"reflect"
	"testing"
	"unsafe"

	"github.com/stretchr/testify/require"
	"gitlab.com/rigel314/pakparse"
)

func TestWidthAndCount(t *testing.T) {
	type Packet struct {
		V []int32 `pakparse:"width=3,count=5"`
	}

	p := mustCreateParser(t, Packet{}, nil)

	s := new(pakparse.State)
	mustConsume(t, p, s, []byte{0x00, 0x00, 0x01})
	mustConsume(t, p, s, []byte{0x00, 0x01, 0x00})
	mustConsume(t, p, s, []byte{0x00, 0x01, 0x01})
	mustConsume(t, p, s, []byte{0x01, 0x00, 0x00})
	mustConsume(t, p, s, []byte{0x01, 0x00, 0x01})

	requirePacket(t, s, Packet{
		[]int32{1, 256, 257, 65536, 65537},
	})
}

func TestInvalidLenTag(t *testing.T) {
	_, err := pakparse.ParseField(reflect.TypeOf(struct{}{}), reflect.StructField{
		Index: []int{0},
		Type:  reflect.TypeOf([]byte{}),
		Tag:   `pakparse:"len=Len"`,
	})
	require.EqualError(t, err, `invalid tag argument "len=Len": "Len" is not a number or the name of a field`)
}

func TestInvalidEndianTag(t *testing.T) {
	_, err := pakparse.ParseField(nil, reflect.StructField{
		Index: []int{0},
		Type:  reflect.TypeOf(uint16(0)),
		Tag:   `pakparse:"endian=blar"`,
	})
	require.EqualError(t, err, `invalid tag argument "endian=blar": invalid endianness "blar"`)
}

func TestInvalidDataTag(t *testing.T) {
	_, err := pakparse.ParseField(nil, reflect.StructField{
		Index: []int{0},
		Type:  reflect.TypeOf(uint16(0)),
		Tag:   `pakparse:"value=blar"`,
	})
	require.Error(t, err)
	require.EqualError(t, err, `invalid tag argument "value=blar": failed to convert "blar" to a value`)
}

func TestArrayFieldSize(t *testing.T) {
	var tests = map[string]struct {
		Type interface{}
		Size int
	}{
		"Byte":    {[2]byte{}, 2},
		"Bool":    {[2]bool{}, 2},
		"Int32":   {[2]int32{}, 8},
		"Float64": {[2]float64{}, 16},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			field, err := pakparse.ParseField(nil, reflect.StructField{
				Index: []int{0},
				Type:  reflect.TypeOf(test.Type),
			})

			require.NoError(t, err, "failed to create parser for %T", test.Type)

			size, ok := field.Size()
			require.True(t, ok, "parser for %T does not have definite size", test.Type)
			require.Equal(t, test.Size, size, "parser for %T does not have expected size", test.Type)
		})
	}
}

func TestBasicTypeField(t *testing.T) {
	type CustomBadInt uint

	var tests = map[string]struct {
		Type   interface{}
		Expect string
	}{
		"Bool":       {bool(false), ""},
		"Int":        {int(1), "integer types with machine-specific widths are unsupported"},
		"Int8":       {int8(1), ""},
		"Int16":      {int16(1), ""},
		"Int32":      {int32(1), ""},
		"Int64":      {int64(1), ""},
		"Uint":       {uint(1), "integer types with machine-specific widths are unsupported"},
		"Uint8":      {uint8(1), ""},
		"Uint16":     {uint16(1), ""},
		"Uint32":     {uint32(1), ""},
		"Uint64":     {uint64(1), ""},
		"Float32":    {float32(1), ""},
		"Float64":    {float64(1), ""},
		"Complex64":  {complex64(1), ""},
		"Complex128": {complex128(1), ""},

		"String": {"1", ""},
		"Array":  {[1]float64{1}, ""},
		"Slice":  {[]float64{1}, ""},
		"Struct": {struct{ X float64 }{1}, ""},

		"Uintptr": {uintptr(1), "fields of type kind uintptr are unsupported"},
		"Chan":    {make(chan int), "fields of type kind chan are unsupported"},
		"Func":    {func() {}, "fields of type kind func are unsupported"},
		"Map":     {map[string]string{}, "fields of type kind map are unsupported"},

		"CustomBadInt":  {CustomBadInt(1), "integer types with machine-specific widths are unsupported"},
		"UnsafePointer": {unsafe.Pointer(nil), "fields of type kind unsafe.Pointer are unsupported"},

		// "Interface":     {nil, false},

		"Ptr/Bool":       {(*bool)(nil), "fields of type *bool are unsupported"},
		"Ptr/Int":        {(*int)(nil), "fields of type *int are unsupported"},
		"Ptr/Int8":       {(*int8)(nil), "fields of type *int8 are unsupported"},
		"Ptr/Int16":      {(*int16)(nil), "fields of type *int16 are unsupported"},
		"Ptr/Int32":      {(*int32)(nil), "fields of type *int32 are unsupported"},
		"Ptr/Int64":      {(*int64)(nil), "fields of type *int64 are unsupported"},
		"Ptr/Uint":       {(*uint)(nil), "fields of type *uint are unsupported"},
		"Ptr/Uint8":      {(*uint8)(nil), "fields of type *uint8 are unsupported"},
		"Ptr/Uint16":     {(*uint16)(nil), "fields of type *uint16 are unsupported"},
		"Ptr/Uint32":     {(*uint32)(nil), "fields of type *uint32 are unsupported"},
		"Ptr/Uint64":     {(*uint64)(nil), "fields of type *uint64 are unsupported"},
		"Ptr/Float32":    {(*float32)(nil), "fields of type *float32 are unsupported"},
		"Ptr/Float64":    {(*float64)(nil), "fields of type *float64 are unsupported"},
		"Ptr/Complex64":  {(*complex64)(nil), "fields of type *complex64 are unsupported"},
		"Ptr/Complex128": {(*complex128)(nil), "fields of type *complex128 are unsupported"},

		"Slice/Bool":       {[]bool{}, ""},
		"Slice/Int":        {[]int{}, "integer types with machine-specific widths are unsupported"},
		"Slice/Int8":       {[]int8{}, ""},
		"Slice/Int16":      {[]int16{}, ""},
		"Slice/Int32":      {[]int32{}, ""},
		"Slice/Int64":      {[]int64{}, ""},
		"Slice/Uint":       {[]uint{}, "integer types with machine-specific widths are unsupported"},
		"Slice/Uint8":      {[]uint8{}, ""},
		"Slice/Uint16":     {[]uint16{}, ""},
		"Slice/Uint32":     {[]uint32{}, ""},
		"Slice/Uint64":     {[]uint64{}, ""},
		"Slice/Float32":    {[]float32{}, ""},
		"Slice/Float64":    {[]float64{}, ""},
		"Slice/Complex64":  {[]complex64{}, ""},
		"Slice/Complex128": {[]complex128{}, ""},
		"Slice/Nested":     {[][]byte{}, "nested arrays and slices are unsupported"},

		"Array/Bool":       {[1]bool{}, ""},
		"Array/Int":        {[1]int{}, "integer types with machine-specific widths are unsupported"},
		"Array/Int8":       {[1]int8{}, ""},
		"Array/Int16":      {[1]int16{}, ""},
		"Array/Int32":      {[1]int32{}, ""},
		"Array/Int64":      {[1]int64{}, ""},
		"Array/Uint":       {[1]uint{}, "integer types with machine-specific widths are unsupported"},
		"Array/Uint8":      {[1]uint8{}, ""},
		"Array/Uint16":     {[1]uint16{}, ""},
		"Array/Uint32":     {[1]uint32{}, ""},
		"Array/Uint64":     {[1]uint64{}, ""},
		"Array/Float32":    {[1]float32{}, ""},
		"Array/Float64":    {[1]float64{}, ""},
		"Array/Complex64":  {[1]complex64{}, ""},
		"Array/Complex128": {[1]complex128{}, ""},
		"Array/Nested":     {[1][1]byte{}, "nested arrays and slices are unsupported"},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			_, err := pakparse.ParseField(nil, reflect.StructField{
				Index: []int{0},
				Type:  reflect.TypeOf(test.Type),
			})

			if test.Expect == "" {
				require.NoError(t, err)
			} else {
				require.EqualError(t, err, test.Expect)
			}
		})
	}
}

func TestCustomTypeField(t *testing.T) {
	type (
		Int       uint8
		ByteArray [2]byte
		ByteSlice []byte

		IntPacket       struct{ V Int }
		IntSlicePacket  struct{ V []Int }
		ByteArrayPacket struct{ V ByteArray }
		ByteSlicePacket struct{ V ByteSlice }
	)

	var tests = map[string]struct {
		Type, Actual, Expect interface{}
		Data                 []byte
		Config               *pakparse.Config
	}{
		"Int": {
			IntPacket{},
			&IntPacket{},
			&IntPacket{0x54},
			[]byte{0x54},
			nil,
		},
		"IntSlice": {
			IntSlicePacket{},
			&IntSlicePacket{},
			&IntSlicePacket{[]Int{0x54}},
			[]byte{0x54},
			&pakparse.Config{
				Count: map[string]interface{}{"V": func(*IntSlicePacket) int { return 1 }},
			},
		},
		"ByteArray": {
			ByteArrayPacket{},
			&ByteArrayPacket{},
			&ByteArrayPacket{ByteArray{0xDE, 0xAD}},
			[]byte{0xDE, 0xAD},
			nil,
		},
		"ByteSlice": {
			ByteSlicePacket{},
			&ByteSlicePacket{},
			&ByteSlicePacket{ByteSlice{0xDE, 0xAD}},
			[]byte{0xDE, 0xAD},
			&pakparse.Config{
				Count: map[string]interface{}{"V": func(*ByteSlicePacket) int { return 2 }},
			},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			p := mustCreateParser(t, test.Type, test.Config)
			err := pakparse.Unmarshal(p, test.Data, test.Actual)
			require.NoError(t, err)

			require.Equal(t, test.Expect, test.Actual)
		})
	}
}
