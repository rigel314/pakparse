module gitlab.com/rigel314/pakparse

go 1.13

require (
	github.com/stretchr/testify v1.3.0
	gitlab.com/go-utils/io v0.1.0
	golang.org/x/xerrors v0.0.0-20190717185122-a985d3407aa7
)
