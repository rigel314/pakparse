/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// package.go: Package documentation

/*
Package pakparse parses packets out of byte streams.

Note

This is a work in progress.  The API may change.  It should be usable for any scenarios similar to those
tested in pakparse_test.go.

Basics

This package has a lot of overlap with "encoding/binary", if your application can use that, it probably
should.

This package supports finding known patterns in a binary stream, and variable length fields.

There are two main functions.  New, which creates a parser based on an output channel of any struct type.
And Run, which reads a channel of []byte and writes whole structs to the output channel.

Controlling the parser

The parser created by New is influenced by tags on your struct.
New looks for the "pakparse" entry in a standard reflect.StructTag string, then looks at the internal entries.
Valid entries are:
	data
This field must exactly match the given data.  data is a hex string beginning with '0x'.  It can be larger
than a uint64.
	len
This field has this length in bytes, except when this field is a slice, where it means number of elements in
the slice.
	vlen
This field has a variable length in bytes found in an earlier field with given name.
	maxlen
This variable length field cannot exceed given length in bytes.
	endian
This field should be decoded with given byte order.  Big endian is the default.
Valid values are 'little', 'big'.

	type ExampleVariablePacket struct {
		Magic   uint16 `pakparse:"value=0x1337"`
		Len     uint16
		Payload []byte `pakparse:"len=Len,maxlen=260"`
		Crc     uint16
	}
In the above case, the Magic field is specified to contain a literal 0x1337, and the default big endian means
the input bytes must be in order: {0x13, 0x37}.  The parser will drop bytes until it finds that pattern.  Len
is just the next two bytes after it finds Magic.  It will interpret Len as a uint16 and immediately return to
searching for Magic if Len exceeds 260.  Otherwise, the parser will read bytes into Payload until it has read
Len bytes.  Crc will just be the next two bytes.  Then the parser will call a user defined check function, if
present.  If there isn't a check function or if the check function returns true, the parser writes the
decoded struct to the output channel.  Otherwise, it drops the packet and returns to searching for Magic.

Tag Specifics

If no pakparse tag is set, the parser will pull sizeof(field type) bytes from the stream.

If data is given, you inherently set a fixed length to len(provided data).

If len is given, it overrides default length behavior with the fixed length given.  A length shorter than a
default will pad the output with zeros.  (e.g. reading 3 bytes into a uint32)  A length larger than a default
is undefined.

Mixing len and vlen is undefined.

Mixing len and maxlen is undefined.

Unexported struct fields are completely ignored, even if they have a pakparse tag.

Known Limitations

pakparse cannot handle bitpacked fields or optional fields.

Nested structs are not handled.
*/
package pakparse
