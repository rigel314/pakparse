/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// packets_test.go: Packet types used for testing

package pakparse_test

type SimplePacket struct {
	Magic uint16 `pakparse:"value=0x1337"`
	A     float64
	B     float64
	C     float64
	D     float64
	Crc   uint16
}

type EmbeddedFieldPacket struct {
	SimplePacket
}

type SimpleSyncPacket struct {
	Sync uint32 `pakparse:"value=0x1337ABCD"`
}

type SimpleSlicePacket struct {
	Magic uint16    `pakparse:"value=0x1337"`
	Q     []float64 `pakparse:"count=4"`
	Crc   uint16
}

type VariablePacket struct {
	Magic   uint16 `pakparse:"value=0x1337"`
	Len     uint16
	Payload []byte `pakparse:"len=Len,maxlen=260"`
	Crc     uint16
}

type DelimiterPacket struct {
	Start    uint8 `pakparse:"value=0x7E"`
	PacketID uint8
	Payload  []byte `pakparse:"maxlen=20"`
	Checksum uint16
	End      uint8 `pakparse:"value=0x7E"`
}

// TODO: test doubled stop byte

type DelimiterPacketEscape struct {
	Start    uint8 `pakparse:"value=0x7E,escape;'0x80,0xDE,0xD0'"`
	PacketID uint8
	Payload  []byte
	Checksum uint16
	End      uint8 `pakparse:"value=0x7E"`
}

type TextPacket struct {
	Line string
	EOL  uint8 `pakparse:"value=0x10"`
}

type UnicodeTextPacket struct {
	Line string `pakparse:"characterset=utf8"`
	EOL  uint8  `pakparse:"value=0x10"`
}

type EndianTestPacket struct {
	D1 uint16
	D2 uint16 `pakparse:"little-endian"`
}

type FixedEndianTestPacket struct {
	D1 uint16 `pakparse:"value=0x1234"`
	D2 uint16 `pakparse:"value=0x1234, little-endian"`
}

type WideIntPacket struct {
	D1 uint32 `pakparse:"value=0x123456, len=3"`
}
