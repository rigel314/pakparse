/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// states.go: Packet parser initialization

package pakparse

import (
	"fmt"
	"reflect"
)

// A Parser captures the information necessary to parse packets
//
// Each parser is tied to a specific packet type
type Parser struct {
	typ    reflect.Type
	fields []FieldParser
	check  CheckFunction
}

// Type returns the packet type that this parser can parse
func (p *Parser) Type() reflect.Type { return p.typ }

// NumField returns the number of fields
func (p *Parser) NumField() int { return len(p.fields) }

// Field returns the Ith field of the parser
func (p *Parser) Field(i int) FieldParser { return p.fields[i] }

// Status represents the status of parsing a packet
type Status int

const (
	// Invalid indicates that invalid data were received and the current parsing state should be discarded
	Invalid Status = iota
	// Incomplete indicates that the current field being parsed is incomplete and needs more data
	Incomplete
	// Complete indicates that the current field being parsed is complete and does not need more data
	Complete
)

// String returns the name of `s`, or "Status:#"
func (s Status) String() string {
	switch s {
	case Invalid:
		return "Invalid"
	case Incomplete:
		return "Incomplete"
	case Complete:
		return "Complete"
	default:
		return fmt.Sprintf("Status:%d", s)
	}
}

// ErrExpectedStructType is returned if New is called on a type other than a struct type
var ErrExpectedStructType = fmt.Errorf("expected a struct type")

// New creates a new parser for TYP using CONFIG. Unexported fields are skipped.
//
// If a check function is specified, it is passed to VerifyCheckFunction. If
// successful, the (wrapped) check function is called each time a complete
// packet is decoded. If the check function returns false, the packet is
// discarded.
//
// For each variable-length field, if a length function is specified, it is
// passed to VerifyLengthFunction. If successful, the (wrapped) length function
// is called immediately before the field is parsed. The partially parsed packet
// is passed to the length function. Specifying a length function for a
// fixed-length field is undefined. Specifying a length function will override
// the `vlen` tag.
//
// For each field, if a parser function is specified, it is passed to
// VerifyParserFunction. If successful, the (wrapped) parser function is used to
// parse the field, overriding the default behavior.
//
// Indeterminate-length fields (variable-length fields with no length function
// and no `len` or `vlen` tag) must be followed by a fixed-value field. The
// fixed-value field is used as a stop sequence for the indeterminate-length
// field. Fixed-length fields between the indeterminate-length field and the
// fixed-value field are allowed. Variable-length fields in-between are not.
// After creating a FieldParser for each field, New verifies these constraints,
// and updates the indeterminate-length fields with their stop sequences.
func New(typ reflect.Type, config *Config) (*Parser, error) {
	if typ.Kind() != reflect.Struct {
		return nil, ErrExpectedStructType
	}

	var err error
	var N = typ.NumField()
	var s = &Parser{
		typ:    typ,
		fields: make([]FieldParser, 0, N),

		check: func(v reflect.Value, d []byte) bool { return true },
	}

	config = config.Safe()
	if config.Check != nil {
		s.check, err = VerifyCheckFunction(typ, config.Check)
		if err != nil {
			return nil, err
		}
	}

	for i := 0; i < N; i++ {
		field := typ.Field(i)

		// skip unexported fields
		if field.PkgPath != "" {
			continue
		}

		// parse field tag
		fconfig, err := ParseFieldConfig(typ, field)
		if err != nil {
			return nil, err
		}

		if fconfig.Omit {
			continue
		}

		if err := setFieldFunc(typ, field, config.Parser, fconfig.SetParser); err != nil {
			return nil, err
		}
		if err := setFieldFunc(typ, field, config.Width, fconfig.SetWidthFunc); err != nil {
			return nil, err
		}
		if err := setFieldFunc(typ, field, config.Count, fconfig.SetCountFunc); err != nil {
			return nil, err
		}

		f, err := NewField(typ, field, fconfig)
		if err != nil {
			return nil, err
		}
		s.fields = append(s.fields, f)
	}

	type needsStop interface {
		FieldParser
		SetStop(intermediate int, sequence []byte)
	}

	// resolve stop sequences
	var v needsStop
	var w int
	for _, field := range s.fields {
		if v == nil {
			v, _ = field.(needsStop)
			continue
		}

		stop, ok := field.(*fixedValueFieldParser)
		if ok {
			v.SetStop(w, stop.value)
			w = 0
			v = nil
			continue
		}

		size, ok := field.Size()
		if !ok {
			return nil, fmtErrorf("field %s: expected fixed byte field (stop sequence), found another variable length field", v.Name())
		}

		w += size
	}
	if v != nil {
		return nil, fmtErrorf("field %s: expected fixed byte field (stop sequence), reached end of structure", v.Name())
	}

	return s, nil
}

func setFieldFunc(typ reflect.Type, field reflect.StructField, fns map[string]interface{}, set func(reflect.Type, interface{}) error) error {
	fn, ok := fns[field.Name]
	if !ok {
		return nil
	}

	p, ok := fn.(*Parser)
	if ok {
		fn = p.AsFieldParser(field)
	}

	return set(typ, fn)
}

// AsFieldMarshaller returns a Marshaller that will marshal a given field using
// `p`.
func (p *Parser) AsFieldMarshaller(field reflect.StructField) Marshaller {
	ftyp := field.Type
	if ftyp.Kind() == reflect.Ptr {
		ftyp = ftyp.Elem()
	}

	if !ftyp.AssignableTo(p.typ) {
		panic(fmtErrorf("parser for type %v cannot be used for field %s of type %v", p.typ, field.Name, field.Type))
	}

	return func(packet reflect.Value) []byte {
		f := packet.Elem().FieldByIndex(field.Index)

		b, err := new(State).Produce(p, reflect.Indirect(f))
		if err != nil {
			panic(err)
		}
		return b
	}
}

// AsFieldParser returns a ParserFunction that will parse a given field using
// `p`.
func (p *Parser) AsFieldParser(field reflect.StructField) ParserFunction {
	ftyp := field.Type
	ptr := false
	if ftyp.Kind() == reflect.Ptr {
		ftyp = ftyp.Elem()
		ptr = true
	}

	if !ftyp.AssignableTo(p.typ) {
		panic(fmtErrorf("parser for type %v cannot be used for field %s of type %v", p.typ, field.Name, field.Type))
	}

	var set = func(packet, value reflect.Value) {
		packet.Elem().FieldByIndex(field.Index).Set(value)
	}

	if ptr {
		set = func(packet, value reflect.Value) {
			f := packet.Elem().FieldByIndex(field.Index)
			f.Set(reflect.New(ftyp))
			f.Elem().Set(value)
		}
	}

	return func(reset bool, data []byte, state reflect.Value, packet reflect.Value) (int, reflect.Value, Status) {
		var s *State
		if !state.IsValid() {
			s = new(State)
		} else {
			s = state.Interface().(*State)
		}

		if reset {
			s.Reset(p)
			return 0, reflect.ValueOf(s), Invalid
		}

		n, unread, status := s.StepPacket(p, data)
		if len(unread) > 0 || n < 0 {
			panic(fmtErrorf("cannot unwind bytes in a custom parser function"))
		}

		if status != Complete {
			return n, reflect.ValueOf(s), status
		}

		v, ok := s.Next()
		if !ok {
			return n, reflect.ValueOf(nil), Invalid
		}

		set(packet, reflect.ValueOf(v))
		return n, reflect.ValueOf(s), Complete
	}
}
