/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// parser_config.go: Parser configuration

package pakparse

// Config allows for tuning and customization of packet parsing
type Config struct {
	// Check is the check function. See VerifyCheckFunction.
	Check interface{}

	// Width is a map of field name => field width function. See VerifyLengthFunction.
	Width map[string]interface{}

	// Count is a map of field name => field count function. See VerifyLengthFunction.
	Count map[string]interface{}

	// Parser is a map of field name => field parser function. See VerifyParserFunction.
	Parser map[string]interface{}
}

// Safe ensures that collection properties of C are non-nil and returns C. If C
// is nil, Safe returns a new Config.
func (c *Config) Safe() *Config {
	if c == nil {
		c = new(Config)
	}
	if c.Width == nil {
		c.Width = map[string]interface{}{}
	}
	if c.Count == nil {
		c.Count = map[string]interface{}{}
	}
	if c.Parser == nil {
		c.Parser = map[string]interface{}{}
	}
	return c
}

// SetCheck sets the check function and returns C. See VerifyCheckFunction.
func (c *Config) SetCheck(fn interface{}) *Config {
	c.Check = fn
	return c
}

// SetWidth sets the width function for a field and returns C. See VerifyLengthFunction.
func (c *Config) SetWidth(name string, fn interface{}) *Config {
	c.Safe()
	c.Width[name] = fn
	return c
}

// SetCount sets the count function for a field and returns C. See VerifyLengthFunction.
func (c *Config) SetCount(name string, fn interface{}) *Config {
	c.Safe()
	c.Count[name] = fn
	return c
}

// SetParser sets the parser function for a field and returns C. See VerifyParserFunction.
func (c *Config) SetParser(name string, fn interface{}) *Config {
	c.Safe()
	c.Parser[name] = fn
	return c
}
