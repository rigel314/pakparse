/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// reflect.go: Reflection-based value un/marshalling

package pakparse

import (
	"encoding/binary"
	"fmt"
	"math"
	"reflect"
)

// A Marshaller converts a (specific type of) value to bytes
type Marshaller func(reflect.Value) []byte

// An Unmarshaller converts bytes to a (specific type of) value
type Unmarshaller func([]byte, reflect.Value)

// NewValueMarshaller returns a marshaller for `typ` using byte ordering
// `order`. If `typ` is not a supported type, NewValueMarshaller will panic.
//
// NewValueMarshaller does not currently support strings or pointers.
//
// NewValueMarshaller does not and will not support channels, functions,
// interfaces, maps, uintptr, or unsafe pointers.
//
// NewValueMarshaller does not support integers with platform-specific sizes,
// e.g. `int` and `uint`.
//
// NewValueMarshaller supports booleans, numeric types (excepting `int` and
// `uint`), slices and arrays of supported types, and named types where the
// underlying type is supported (e.g. `type Custom uint16`).
//
// If specified, `width` overrides the width of array and slice elements.
// `width` is ignored for other types. NewValueMarshaller will panic if `width`
// is greater than sizeof(typ.Elem()).
//
// NewValueMarshaller has a fast path for `[]byte`; the resulting marshaller
// simply calls `value.Bytes()`. NewValueMarshaller has a fast path for
// `[...]byte`; the resulting marshaller creates a new slice with sufficient
// capacity and copies the array to the new slice. `byte` and `uint8` are
// interchangable. These fast paths will work for named types with an underlying
// type of `[]byte`. They will not work for slices and arrays with an element
// type that is not `byte`, regardless of the size of the element type, even if
// the element type is `type Custom byte`. This limitation is imposed by Go's
// type system, not by this package.
func NewValueMarshaller(typ reflect.Type, order binary.ByteOrder, width int) Marshaller {
	switch typ.Kind() {
	case reflect.Bool:
		return func(v reflect.Value) []byte {
			if v.Bool() {
				return []byte{1}
			}
			return []byte{0}
		}

	case reflect.Int8:
		return func(v reflect.Value) []byte { return []byte{uint8(v.Int())} }
	case reflect.Uint8:
		return func(v reflect.Value) []byte { return []byte{uint8(v.Uint())} }

	case reflect.Int16:
		return bytePutter(2, func(b []byte, v reflect.Value) { order.PutUint16(b, uint16(v.Int())) })
	case reflect.Uint16:
		return bytePutter(2, func(b []byte, v reflect.Value) { order.PutUint16(b, uint16(v.Uint())) })

	case reflect.Int32:
		return bytePutter(4, func(b []byte, v reflect.Value) { order.PutUint32(b, uint32(v.Int())) })
	case reflect.Uint32:
		return bytePutter(4, func(b []byte, v reflect.Value) { order.PutUint32(b, uint32(v.Uint())) })

	case reflect.Int64:
		return bytePutter(8, func(b []byte, v reflect.Value) { order.PutUint64(b, uint64(v.Int())) })
	case reflect.Uint64:
		return bytePutter(8, func(b []byte, v reflect.Value) { order.PutUint64(b, uint64(v.Uint())) })

	case reflect.Float32:
		return bytePutter(4, func(b []byte, v reflect.Value) { order.PutUint32(b, math.Float32bits(float32(v.Float()))) })

	case reflect.Float64:
		return bytePutter(8, func(b []byte, v reflect.Value) { order.PutUint64(b, math.Float64bits(v.Float())) })

	case reflect.Complex64:
		return bytePutter(8, func(b []byte, v reflect.Value) {
			c := complex64(v.Complex())
			order.PutUint32(b[4:], math.Float32bits(real(c)))
			order.PutUint32(b[:4], math.Float32bits(imag(c)))
		})

	case reflect.Complex128:
		return bytePutter(16, func(b []byte, v reflect.Value) {
			c := v.Complex()
			order.PutUint64(b[8:], math.Float64bits(real(c)))
			order.PutUint64(b[:8], math.Float64bits(imag(c)))
		})

	case reflect.Slice:
		eltyp := typ.Elem()
		elget := NewValueMarshaller(eltyp, order, -1)

		S := int(eltyp.Size())
		if width > 0 {
			if width > S {
				panic(fmtErrorf("width %d is greater than sizeof(%v)", width, eltyp))
			}
			S = width
		}

		if eltyp.Kind() == reflect.Uint8 && !needsConversion(eltyp) {
			return reflect.Value.Bytes
		}

		return func(v reflect.Value) []byte {
			b := make([]byte, 0, S*v.Len())

			N := v.Len()
			for i := 0; i < N; i++ {
				b = append(b, elget(v.Index(i))...)
			}
			return b
		}

	case reflect.Array:
		eltyp := typ.Elem()
		elget := NewValueMarshaller(eltyp, order, -1)

		N := typ.Len()
		S := int(eltyp.Size())
		if width > 0 {
			if width > S {
				panic(fmtErrorf("width %d is greater than sizeof(%v)", width, eltyp))
			}
			S = width
		}

		if eltyp.Kind() == reflect.Uint8 && !needsConversion(eltyp) {
			return func(v reflect.Value) []byte {
				b := make([]byte, N)
				reflect.Copy(reflect.ValueOf(b), v)
				return b
			}
		}

		return func(v reflect.Value) []byte {
			b := make([]byte, 0, S*N)

			for i := 0; i < N; i++ {
				b = append(b, elget(v.Index(i))...)
			}
			return b
		}

	case reflect.String:
		panic(fmtErrorf("// TODO: handle String"))

	default:
		panic(fmt.Sprintf("kind %s is unsupported", typ.Kind()))
	}
}

// NewValueUnmarshaller returns an unmarshaller for `typ` using byte ordering
// `order`. If `typ` is not a supported type, NewValueUnmarshaller will panic.
//
// NewValueUnmarshaller does not currently support strings or pointers.
//
// NewValueUnmarshaller does not and will not support channels, functions,
// interfaces, maps, uintptr, or unsafe pointers.
//
// NewValueUnmarshaller does not support integers with platform-specific sizes,
// e.g. `int` and `uint`.
//
// NewValueUnmarshaller supports booleans, numeric types (excepting `int` and
// `uint`), slices and arrays of supported types, and named types where the
// underlying type is supported (e.g. `type Custom uint16`).
//
// If specified, `width` overrides the width of array and slice elements.
// `width` is ignored for other types. NewValueUnmarshaller will panic if
// `width` is greater than sizeof(typ.Elem()).
//
// NewValueUnmarshaller has a fast path for `[]byte`; the resulting marshaller
// simply calls `value.Bytes()`. NewValueUnmarshaller has a fast path for
// `[...]byte`; the resulting marshaller creates a new slice with sufficient
// capacity and copies the array to the new slice. `byte` and `uint8` are
// interchangable. These fast paths will work for named types with an underlying
// type of `[]byte`. They will not work for slices and arrays with an element
// type that is not `byte`, regardless of the size of the element type, even if
// the element type is `type Custom byte`. This limitation is imposed by Go's
// type system, not by this package.
func NewValueUnmarshaller(typ reflect.Type, order binary.ByteOrder, width int) Unmarshaller {
	var pad func([]byte) []byte
	switch typ.Kind() {
	case reflect.Int16, reflect.Uint16:
		pad = newPadder(2, order)
	case reflect.Int32, reflect.Uint32:
		pad = newPadder(4, order)
	case reflect.Int64, reflect.Uint64:
		pad = newPadder(8, order)
	}

	switch typ.Kind() {
	case reflect.Bool:
		return func(b []byte, v reflect.Value) { v.SetBool(bool(b[0] != 0)) }
	case reflect.Uint8:
		return func(b []byte, v reflect.Value) { v.SetUint(uint64(b[0])) }
	case reflect.Uint16:
		return func(b []byte, v reflect.Value) { v.SetUint(uint64(order.Uint16(pad(b)))) }
	case reflect.Uint32:
		return func(b []byte, v reflect.Value) { v.SetUint(uint64(order.Uint32(pad(b)))) }
	case reflect.Uint64:
		return func(b []byte, v reflect.Value) { v.SetUint(uint64(order.Uint64(pad(b)))) }
	case reflect.Int8:
		return func(b []byte, v reflect.Value) { v.SetInt(int64(b[0])) }
	case reflect.Int16:
		return func(b []byte, v reflect.Value) { v.SetInt(int64(order.Uint16(pad(b)))) }
	case reflect.Int32:
		return func(b []byte, v reflect.Value) { v.SetInt(int64(order.Uint32(pad(b)))) }
	case reflect.Int64:
		return func(b []byte, v reflect.Value) { v.SetInt(int64(order.Uint64(pad(b)))) }

	case reflect.Float32:
		return func(b []byte, v reflect.Value) { v.SetFloat(float64(math.Float32frombits(order.Uint32(b)))) }
	case reflect.Float64:
		return func(b []byte, v reflect.Value) { v.SetFloat(math.Float64frombits(order.Uint64(b))) }

	case reflect.Complex64:
		return func(b []byte, v reflect.Value) {
			v.SetComplex(complex(
				float64(math.Float32frombits(order.Uint32(b[:4]))),
				float64(math.Float32frombits(order.Uint32(b[4:])))))
		}
	case reflect.Complex128:
		return func(b []byte, v reflect.Value) {
			v.SetComplex(complex(
				math.Float64frombits(order.Uint64(b[:8])),
				math.Float64frombits(order.Uint64(b[8:]))))
		}

	case reflect.Slice:
		eltyp := typ.Elem()
		elset := NewValueUnmarshaller(eltyp, order, -1)

		S := int(eltyp.Size())
		if width > 0 {
			if width > S {
				panic(fmtErrorf("width %d is greater than sizeof(%v)", width, eltyp))
			}
			S = width
		}

		if eltyp.Kind() == reflect.Uint8 && !needsConversion(eltyp) {
			return func(b []byte, v reflect.Value) { v.SetBytes(b) }
		}

		return func(b []byte, v reflect.Value) {
			if len(b)%S != 0 {
				panic(fmtErrorf("data size is not a multiple of element size"))
			}

			N := len(b) / S
			v.Set(reflect.MakeSlice(typ, N, N))

			for i := 0; i < N; i++ {
				elset(b[i*S:(i+1)*S], v.Index(i))
			}
		}

	case reflect.Array:
		eltyp := typ.Elem()
		elset := NewValueUnmarshaller(eltyp, order, -1)

		N := typ.Len()
		S := int(eltyp.Size())
		if width > 0 {
			if width > S {
				panic(fmtErrorf("width %d is greater than sizeof(%v)", width, eltyp))
			}
			S = width
		}

		if eltyp.Kind() == reflect.Uint8 && !needsConversion(eltyp) {
			return func(b []byte, v reflect.Value) {
				if len(b) > N {
					panic(fmtErrorf("data size exceeds the size of the array"))
				}
				reflect.Copy(v, reflect.ValueOf(b))
			}
		}

		return func(b []byte, v reflect.Value) {
			if len(b)%N != 0 {
				panic(fmtErrorf("data size is not divisible by array length"))
			}

			Z := len(b) / N
			if Z > S {
				panic(fmtErrorf("data size exceeds the product of array length and array element size"))
			}

			for i := 0; i < N; i++ {
				elset(b[i*Z:(i+1)*Z], v.Index(i))
			}
		}

	case reflect.String:
		panic(fmtErrorf("// TODO: handle String"))

	default:
		panic(fmt.Sprintf("kind %s is unsupported", typ.Kind()))
	}
}

func newPadder(size int, order binary.ByteOrder) func([]byte) []byte {
	switch order {
	case binary.BigEndian:
		return func(b []byte) []byte { return zeroPadLeading(b, size) }
	case binary.LittleEndian:
		return func(b []byte) []byte { return zeroPadTrailing(b, size) }
	}

	panic(fmtErrorf("unsupported byte ordering"))
}

func zeroPadLeading(data []byte, size int) []byte {
	if len(data) >= size {
		return data
	}

	b := make([]byte, size)
	copy(b[size-len(data):], data)
	return b
}

func zeroPadTrailing(data []byte, size int) []byte {
	if len(data) >= size {
		return data
	}

	b := make([]byte, size)
	copy(b, data)
	return b
}

func needsConversion(typ reflect.Type) bool {
	var underlying reflect.Type

	switch typ.Kind() {
	case reflect.Bool:
		underlying = reflect.TypeOf(false)
	case reflect.Uint8:
		underlying = reflect.TypeOf(uint8(0))
	case reflect.Uint16:
		underlying = reflect.TypeOf(uint16(0))
	case reflect.Uint32:
		underlying = reflect.TypeOf(uint32(0))
	case reflect.Uint64:
		underlying = reflect.TypeOf(uint64(0))
	case reflect.Int8:
		underlying = reflect.TypeOf(int8(0))
	case reflect.Int16:
		underlying = reflect.TypeOf(int16(0))
	case reflect.Int32:
		underlying = reflect.TypeOf(int32(0))
	case reflect.Int64:
		underlying = reflect.TypeOf(int64(0))
	case reflect.Float32:
		underlying = reflect.TypeOf(float32(0))
	case reflect.Float64:
		underlying = reflect.TypeOf(float64(0))
	case reflect.Complex64:
		underlying = reflect.TypeOf(complex64(0))
	case reflect.Complex128:
		underlying = reflect.TypeOf(complex128(0))
	case reflect.String:
		underlying = reflect.TypeOf("")

	case reflect.Slice:
		if needsConversion(typ.Elem()) {
			return true
		}
		underlying = reflect.SliceOf(typ.Elem())

	case reflect.Array:
		if needsConversion(typ.Elem()) {
			return true
		}
		underlying = reflect.ArrayOf(typ.Len(), typ.Elem())

	default:
		panic(fmt.Sprintf("kind %s is unsupported", typ.Kind()))
	}

	return !typ.AssignableTo(underlying)
}

func bytePutter(size int, put func([]byte, reflect.Value)) Marshaller {
	return func(v reflect.Value) []byte {
		b := make([]byte, size)
		put(b, v)
		return b
	}
}
