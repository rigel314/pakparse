/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// reflect_test.go: Tests reflection-based value un/marshalling

package pakparse_test

import (
	"encoding/binary"
	"reflect"
	"testing"
	"unsafe"

	"github.com/stretchr/testify/require"
	"gitlab.com/rigel314/pakparse"
)

func TestNewValueMarshallerAndUnmarshaller(t *testing.T) {
	var tests = map[string]struct {
		Value  interface{}
		Expect string
		Data   []byte
	}{
		"Bool":       {bool(false), "", []byte{0}},
		"Int":        {int(1), "kind int is unsupported", nil},
		"Int8":       {int8(1), "", encode{Big}.i1(1)},
		"Int16":      {int16(1), "", encode{Big}.i2(1)},
		"Int32":      {int32(1), "", encode{Big}.i4(1)},
		"Int64":      {int64(1), "", encode{Big}.i8(1)},
		"Uint":       {uint(1), "kind uint is unsupported", nil},
		"Uint8":      {uint8(1), "", encode{Big}.b1(1)},
		"Uint16":     {uint16(1), "", encode{Big}.b2(1)},
		"Uint32":     {uint32(1), "", encode{Big}.b4(1)},
		"Uint64":     {uint64(1), "", encode{Big}.b8(1)},
		"Float32":    {float32(1), "", encode{Big}.f32(1)},
		"Float64":    {float64(1), "", encode{Big}.f64(1)},
		"Complex64":  {complex64(1 + 1i), "", encode{Big}.c64(1 + 1i)},
		"Complex128": {complex128(1 + 1i), "", encode{Big}.c128(1 + 1i)},

		// "String": {"1", "", []byte{}},
		"Array": {[1]float64{1}, "", encode{Big}.f64(1)},
		"Slice": {[]float64{1}, "", encode{Big}.f64(1)},

		"Uintptr":       {uintptr(1), "kind uintptr is unsupported", nil},
		"Chan":          {make(chan int), "kind chan is unsupported", nil},
		"Func":          {func() {}, "kind func is unsupported", nil},
		"Map":           {map[string]string{}, "kind map is unsupported", nil},
		"Struct":        {struct{ X float64 }{1}, "kind struct is unsupported", nil},
		"UnsafePointer": {unsafe.Pointer(nil), "kind unsafe.Pointer is unsupported", nil},
		// "Interface":     {nil, "kind interface is unsupported", nil},

		"Ptr/Bool":       {(*bool)(nil), "kind ptr is unsupported", nil},
		"Ptr/Int":        {(*int)(nil), "kind ptr is unsupported", nil},
		"Ptr/Int8":       {(*int8)(nil), "kind ptr is unsupported", nil},
		"Ptr/Int16":      {(*int16)(nil), "kind ptr is unsupported", nil},
		"Ptr/Int32":      {(*int32)(nil), "kind ptr is unsupported", nil},
		"Ptr/Int64":      {(*int64)(nil), "kind ptr is unsupported", nil},
		"Ptr/Uint":       {(*uint)(nil), "kind ptr is unsupported", nil},
		"Ptr/Uint8":      {(*uint8)(nil), "kind ptr is unsupported", nil},
		"Ptr/Uint16":     {(*uint16)(nil), "kind ptr is unsupported", nil},
		"Ptr/Uint32":     {(*uint32)(nil), "kind ptr is unsupported", nil},
		"Ptr/Uint64":     {(*uint64)(nil), "kind ptr is unsupported", nil},
		"Ptr/Float32":    {(*float32)(nil), "kind ptr is unsupported", nil},
		"Ptr/Float64":    {(*float64)(nil), "kind ptr is unsupported", nil},
		"Ptr/Complex64":  {(*complex64)(nil), "kind ptr is unsupported", nil},
		"Ptr/Complex128": {(*complex128)(nil), "kind ptr is unsupported", nil},

		"Slice/Bool":       {[]bool{true}, "", []byte{1}},
		"Slice/Int":        {[]int{}, "kind int is unsupported", nil},
		"Slice/Int8":       {[]int8{1}, "", encode{Big}.i1(1)},
		"Slice/Int16":      {[]int16{1}, "", encode{Big}.i2(1)},
		"Slice/Int32":      {[]int32{1}, "", encode{Big}.i4(1)},
		"Slice/Int64":      {[]int64{1}, "", encode{Big}.i8(1)},
		"Slice/Uint":       {[]uint{}, "kind uint is unsupported", nil},
		"Slice/Uint8":      {[]uint8{1}, "", encode{Big}.b1(1)},
		"Slice/Uint16":     {[]uint16{1}, "", encode{Big}.b2(1)},
		"Slice/Uint32":     {[]uint32{1}, "", encode{Big}.b4(1)},
		"Slice/Uint64":     {[]uint64{1}, "", encode{Big}.b8(1)},
		"Slice/Float32":    {[]float32{1}, "", encode{Big}.f32(1)},
		"Slice/Float64":    {[]float64{1}, "", encode{Big}.f64(1)},
		"Slice/Complex64":  {[]complex64{1 + 1i}, "", encode{Big}.c64(1 + 1i)},
		"Slice/Complex128": {[]complex128{1 + 1i}, "", encode{Big}.c128(1 + 1i)},

		"Array/Bool":       {[1]bool{true}, "", []byte{1}},
		"Array/Int":        {[1]int{}, "kind int is unsupported", nil},
		"Array/Int8":       {[1]int8{1}, "", encode{Big}.i1(1)},
		"Array/Int16":      {[1]int16{1}, "", encode{Big}.i2(1)},
		"Array/Int32":      {[1]int32{1}, "", encode{Big}.i4(1)},
		"Array/Int64":      {[1]int64{1}, "", encode{Big}.i8(1)},
		"Array/Uint":       {[1]uint{}, "kind uint is unsupported", nil},
		"Array/Uint8":      {[1]uint8{1}, "", encode{Big}.b1(1)},
		"Array/Uint16":     {[1]uint16{1}, "", encode{Big}.b2(1)},
		"Array/Uint32":     {[1]uint32{1}, "", encode{Big}.b4(1)},
		"Array/Uint64":     {[1]uint64{1}, "", encode{Big}.b8(1)},
		"Array/Float32":    {[1]float32{1}, "", encode{Big}.f32(1)},
		"Array/Float64":    {[1]float64{1}, "", encode{Big}.f64(1)},
		"Array/Complex64":  {[1]complex64{1 + 1i}, "", encode{Big}.c64(1 + 1i)},
		"Array/Complex128": {[1]complex128{1 + 1i}, "", encode{Big}.c128(1 + 1i)},
	}

	for name, test := range tests {
		typ := reflect.TypeOf(test.Value)

		t.Run(name, func(t *testing.T) {
			t.Run("Marshaller", func(t *testing.T) {
				if test.Expect == "" {
					requireNotPanics(t, func() {
						fn := pakparse.NewValueMarshaller(typ, binary.BigEndian, -1)
						require.Equal(t, test.Data, fn(reflect.ValueOf(test.Value)))
					})
				} else {
					require.PanicsWithValue(t, test.Expect, func() {
						pakparse.NewValueMarshaller(typ, binary.BigEndian, -1)
					})
				}
			})
			t.Run("Unmarshaller", func(t *testing.T) {
				if test.Expect == "" {
					requireNotPanics(t, func() {
						fn := pakparse.NewValueUnmarshaller(typ, binary.BigEndian, -1)
						v := reflect.New(typ).Elem()
						fn(test.Data, v)
						require.Equal(t, test.Value, v.Interface())
					})
				} else {
					require.PanicsWithValue(t, test.Expect, func() {
						pakparse.NewValueUnmarshaller(typ, binary.BigEndian, -1)
					})
				}
			})
		})
	}
}
