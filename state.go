/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// state.go: Parsing state

package pakparse

import (
	"io"
	"reflect"
)

// Unmarshal parses DATA into V
//
// Unmarshal creates a parser, initializes it, consumes the data, pops the
// parsed packet off the queue, and assigns the packet to V. If data is
// insufficient to parse as a packet, Unmarshal returns io.EOF.
//
// See State.InitWithPointer, State.Consume, State.Next.
func Unmarshal(p *Parser, data []byte, v interface{}) error {
	s := new(State)

	u := reflect.ValueOf(v).Elem()
	if !u.CanSet() {
		return fmtErrorf("cannot set *v")
	}

	s.Consume(p, data)
	x, ok := s.Next()
	if !ok {
		return io.EOF
	}

	u.Set(reflect.ValueOf(x))
	return nil
}

// State represents a particular
type State struct {
	field int // index of current field

	outType    reflect.Type  // packet type
	outVal     reflect.Value // current packet
	outPackets *stateQueue   // packet queue for complete packets

	packet     []byte // bytes of the packet
	fieldState []interface{}

	packetsParsed    int
	bytesConsumed    int
	packetsDiscarded int
	bytesDiscarded   int
	packetsRejected  int

	// Receive can be used to disable the packet queue and receive packets
	// directly
	Receive func(reflect.Value)

	// Discard can be used to capture discarded bytes
	Discard func([]byte)

	// Reject can be used to capture rejected packets
	Reject func(reflect.Value)
}

type stateQueue struct {
	count      int
	head, tail *stateQueueNode
}

type stateQueueNode struct {
	value interface{}
	next  *stateQueueNode
}

func (q *stateQueue) Push(v interface{}) {
	n := &stateQueueNode{v, nil}

	if q.count == 0 {
		q.head, q.tail = n, n
	} else {
		q.tail.next, q.tail = n, n
	}
	q.count++
}

func (q *stateQueue) Pop() (interface{}, bool) {
	if q == nil || q.count == 0 {
		return nil, false
	}

	if q.count == 1 {
		n := q.head
		q.head, q.tail, q.count = nil, nil, 0
		return n.value, true
	}

	n := q.head
	q.head = n.next
	q.count--
	return n.value, true
}

// ensures that `p` is a valid parser and `s` is initialized
func (s *State) ensure(p *Parser) {
	if p == nil || p.typ == nil {
		panic(fmtErrorf("invalid parser"))
	}

	if s.field >= len(p.fields) {
		s.reset(p)
	}

	// don't reinitialize
	if s.outVal.Kind() != reflect.Invalid {
		if s.outType != p.typ {
			panic(fmtErrorf("state has already been initialized with a different parser"))
		}
		return
	}

	s.outType = p.typ
	s.outVal = reflect.New(p.typ)
	s.outPackets = new(stateQueue)
	s.fieldState = make([]interface{}, len(p.fields))
}

// Next pops a packet off the queue and returns it.
func (s *State) Next() (interface{}, bool) {
	return s.outPackets.Pop()
}

// BytesConsumed returns the number of bytes that have been observed
func (s *State) BytesConsumed() int { return s.bytesConsumed }

// BytesDiscarded returns the number of bytes dropped between packets
func (s *State) BytesDiscarded() int { return s.bytesDiscarded }

// PacketsParsed returns the number of full packets parsed successfully
func (s *State) PacketsParsed() int { return s.packetsParsed }

// PacketsDiscarded returns the number of times a partial packet was discarded
func (s *State) PacketsDiscarded() int { return s.packetsDiscarded }

// PacketsRejected returns the number of times a complete packet was rejected
func (s *State) PacketsRejected() int { return s.packetsRejected }

// ResetStats sets the various informational counters to zero
func (s *State) ResetStats() {
	s.packetsParsed = 0
	s.bytesConsumed = 0
	s.packetsDiscarded = 0
	s.bytesDiscarded = 0
	s.packetsRejected = 0
}
