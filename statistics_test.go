/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// statistics_test.go: Tests of parser statistics

package pakparse_test

import (
	"math"
	"reflect"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/rigel314/pakparse"
)

func TestStateRejected(t *testing.T) {
	p := mustCreateParser(t, SimplePacket{}, &pakparse.Config{
		Check: func(s *SimplePacket) bool { return s.Crc == 0xDEAD },
	})

	rejected := false
	s := &pakparse.State{
		Reject: func(reflect.Value) { rejected = true },
	}
	mustConsume(t, p, s, encode{Big}.b2(0x1337))
	mustConsume(t, p, s, encode{Big}.f64(math.Pi))
	mustConsume(t, p, s, encode{Big}.f64(math.Phi))
	mustConsume(t, p, s, encode{Big}.f64(math.E))
	mustConsume(t, p, s, encode{Big}.f64(math.Sqrt2))
	mustConsume(t, p, s, encode{Big}.b2(0xDEAF))

	_, ok := s.Next()
	require.True(t, rejected, "Did not call reject callback")
	require.False(t, ok, "Did not reject invalid packet")
	require.Equal(t, 1, s.PacketsRejected(), "Did not increment reject count")
}

func TestStateDiscarded(t *testing.T) {
	p := mustCreateParser(t, SimpleSyncPacket{}, nil)

	discarded := false
	s := &pakparse.State{
		Discard: func(b []byte) { discarded = true },
	}
	mustConsume(t, p, s, []byte{0x13, 0x37, 0x37, 0xAB, 0xCD, 0x13, 0x37, 0xAB, 0xCD})

	s.Next()
	require.True(t, discarded, "Did not call discard callback")
	require.Equal(t, 1, s.PacketsDiscarded(), "Did not increment discard count")
	require.Equal(t, 5, s.BytesDiscarded(), "Did not discard all bytes")
}

func TestSyncDiscard(t *testing.T) {
	p := mustCreateParser(t, SimplePacket{}, nil)

	s := new(pakparse.State)
	mustConsume(t, p, s, encode{Big}.b1(0xEC))
	mustConsume(t, p, s, encode{Big}.b2(0x1337))
	mustConsume(t, p, s, encode{Big}.f64(math.Pi))
	mustConsume(t, p, s, encode{Big}.f64(math.Phi))
	mustConsume(t, p, s, encode{Big}.f64(math.E))
	mustConsume(t, p, s, encode{Big}.f64(math.Sqrt2))
	mustConsume(t, p, s, encode{Big}.b2(0xDEAD))

	requirePacket(t, s, SimplePacket{
		Magic: 0x1337,
		A:     math.Pi,
		B:     math.Phi,
		C:     math.E,
		D:     math.Sqrt2,
		Crc:   0xDEAD,
	})

	require.Equal(t, 1, s.BytesDiscarded(), "Did not discard bytes")
	require.Equal(t, 0, s.PacketsDiscarded(), "Discarded packet")
}

func TestSyncInvalid(t *testing.T) {
	p := mustCreateParser(t, SimplePacket{}, nil)

	s := new(pakparse.State)
	mustConsume(t, p, s, encode{Big}.b1(0x13))
	mustConsume(t, p, s, encode{Big}.b1(0xEC))

	require.Equal(t, 2, s.BytesDiscarded(), "Did not discard bytes")
	require.Equal(t, 1, s.PacketsDiscarded(), "Did not discard packet")
}
