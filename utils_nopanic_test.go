// +build !panic

package pakparse_test

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/rigel314/pakparse"
)

func mustConsume(t require.TestingT, p *pakparse.Parser, s *pakparse.State, data []byte) {
	helper(t).Helper()

	requireNotPanics(t, func() {
		helper(t).Helper()

		s.Consume(p, data)
	})
}
