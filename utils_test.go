/*
	Copyright 2019 Christopher Creager
	Copyright 2019 Ethan Reesor

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// utils_test.go: Utilities used for testing

package pakparse_test

import (
	"encoding/binary"
	"fmt"
	"math"
	"reflect"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/rigel314/pakparse"
)

func mustRunInTime(t require.TestingT, duration time.Duration, fn func(), args ...interface{}) {
	helper(t).Helper()

	done := make(chan interface{})

	go func() {
		defer func() {
			done <- recover()
		}()
		fn()
	}()

	select {
	case <-time.After(duration):
		t.Errorf("%s", fmt.Sprint(args...))
		t.FailNow()
	case err := <-done:
		if err != nil {
			panic(err)
		}
	}
}

func mustCreateParser(t require.TestingT, typ interface{}, config *pakparse.Config) *pakparse.Parser {
	helper(t).Helper()

	var p *pakparse.Parser
	var err error
	requireNotPanics(t, func() {
		p, err = pakparse.New(reflect.TypeOf(typ), config)
	})
	require.NoError(t, err)
	return p
}

func mustUnmarshal(t require.TestingT, p *pakparse.Parser, data []byte, v interface{}) {
	helper(t).Helper()

	requireNotPanics(t, func() {
		err := pakparse.Unmarshal(p, data, v)
		require.NoError(t, err)
	})
}

func async(fn func()) chan struct{} {
	done := make(chan struct{})
	go func() {
		fn()
		close(done)
	}()
	return done
}

var Little = binary.LittleEndian
var Big = binary.BigEndian

type encode struct{ binary.ByteOrder }

func encodeLit(n int, v func([]byte)) []byte {
	var buf [8]byte
	v(buf[:])
	return buf[:n]
}

func (e encode) b1(v uint8) []byte  { return []byte{v} }
func (e encode) b2(v uint16) []byte { return encodeLit(2, func(b []byte) { e.PutUint16(b, v) }) }
func (e encode) b3(v uint32) []byte { return encodeLit(4, func(b []byte) { e.PutUint32(b, v) })[1:] }
func (e encode) b4(v uint32) []byte { return encodeLit(4, func(b []byte) { e.PutUint32(b, v) }) }
func (e encode) b5(v uint64) []byte { return encodeLit(8, func(b []byte) { e.PutUint64(b, v) })[3:] }
func (e encode) b6(v uint64) []byte { return encodeLit(8, func(b []byte) { e.PutUint64(b, v) })[2:] }
func (e encode) b7(v uint64) []byte { return encodeLit(8, func(b []byte) { e.PutUint64(b, v) })[1:] }
func (e encode) b8(v uint64) []byte { return encodeLit(8, func(b []byte) { e.PutUint64(b, v) }) }
func (e encode) i1(v int8) []byte   { return e.b1(uint8(v)) }
func (e encode) i2(v int16) []byte  { return e.b2(uint16(v)) }
func (e encode) i3(v int32) []byte  { return e.b3(uint32(v)) }
func (e encode) i4(v int32) []byte  { return e.b4(uint32(v)) }
func (e encode) i5(v int64) []byte  { return e.b5(uint64(v)) }
func (e encode) i6(v int64) []byte  { return e.b6(uint64(v)) }
func (e encode) i7(v int64) []byte  { return e.b7(uint64(v)) }
func (e encode) i8(v int64) []byte  { return e.b8(uint64(v)) }

func (e encode) f32(v float32) []byte     { return e.b4(math.Float32bits(v)) }
func (e encode) f64(v float64) []byte     { return e.b8(math.Float64bits(v)) }
func (e encode) c64(v complex64) []byte   { return append(e.f32(real(v)), e.f32(imag(v))...) }
func (e encode) c128(v complex128) []byte { return append(e.f64(real(v)), e.f64(imag(v))...) }

func requirePacket(t require.TestingT, p *pakparse.State, expect interface{}) interface{} {
	helper(t).Helper()

	v, ok := p.Next()
	require.True(t, ok, "Did not decode a packet")

	require.IsType(t, expect, v)
	require.Equal(t, expect, v)

	return v
}

func requireBytes(t require.TestingT, actual []byte, expected ...[]byte) {
	helper(t).Helper()

	var l int
	for _, b := range expected {
		l += len(b)
	}

	ex := make([]byte, 0, l)
	for _, b := range expected {
		ex = append(ex, b...)
	}

	require.Equal(t, ex, actual)
}

type PanicTestFunc func() // required because ??? weird compiler errors

func requirePanicsWithError(t require.TestingT, expected string, fn PanicTestFunc) {
	helper(t).Helper()

	defer func() {
		helper(t).Helper()

		v := recover()
		if v == nil {
			t.Errorf("%s", fmt.Sprintf("func %#v should panic with error:\t%#v", fn, expected))
			t.FailNow()
			return
		}
		err, ok := v.(error)
		if !ok {
			t.Errorf("%s", fmt.Sprintf("func %#v should panic with error:\t%#v\n\tPanic value:\t%#v", fn, expected, v))
			t.FailNow()
			return
		}
		if err.Error() != expected {
			t.Errorf("%s", fmt.Sprintf("func %#v should panic with error:\t%#v\n\tPanic error:\t%#v", fn, expected, err))
			t.FailNow()
			return
		}
	}()

	fn()
}

func requireNotPanics(t require.TestingT, fn PanicTestFunc) {
	helper(t).Helper()

	defer func() {
		helper(t).Helper()

		v := recover()
		if v == nil {
			return
		}

		t.Errorf("%s", fmt.Sprintf("func %#v should not panic\n\tPanic value:\t%+v", fn, v))
		t.FailNow()
	}()

	fn()
}

type tHelper interface {
	Helper()
}

type fakeHelper struct{}

func (fakeHelper) Helper() {}

func helper(t require.TestingT) tHelper {
	h, ok := t.(tHelper)
	if ok {
		return h
	}
	return fakeHelper{}
}
